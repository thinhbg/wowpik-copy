<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::namespace('Web')->group(function () {
    Route::get('/trang-chu', 'IndexController@index');
    Route::get('/', 'IndexController@index');
    Route::get('/dich-vu', 'IndexController@service');
    Route::get('/do-choi', 'ProductController@index')->name('do-choi');
    Route::get('/goc-chia-se', 'BlogController@index')->name('goc-chia-se');
    Route::get('/goc-kheo-tay', 'DiyController@theme')->name('goc-kheo-tay');
    Route::get('/goc-kheo-tay/chu-de/{slug}', 'DiyController@index')->name('chu-de');

    Route::post('/submitGift', 'RegisterController@submitGift')->name('submitGift');
    Route::post('/submitRegister', 'RegisterController@submitRegister')->name('submitRegister');


    Route::get('/tin-tuc/{slug}', "IndexController@tintuc");
    Route::get('/do-choi/{slug}', "ProductController@show");
    Route::get('/goc-chia-se/{slug}', "BlogController@show");
    Route::get('/goc-kheo-tay/{slug}', "DiyController@show");

    Route::get('/tim-kiem', "IndexController@search");
    Route::get('/download', "DiyController@download");

    Route::get('/dang-nhap', "CustomerController@dangnhap");
    Route::post('/dang-nhap', "CustomerController@dangnhapPost")->name('login');

    Route::post('/theo-doi', "CustomerController@theodoi");

    Route::get('/dang-ky', "CustomerController@dangky");
    Route::post('/dang-ky', "CustomerController@dangkyPost")->name('register');

    Route::get('/dang-xuat', "CustomerController@dangxuat");


    Route::middleware('auth')->group(function () {
        Route::get('/thong-tin', "CustomerController@thongtin");
        Route::post('/thongtinPost', "CustomerController@thongtinPost");
    });

    Route::get('/test_mail', function () {
        $testRegis = \App\Models\Registrations::first();
        $testRegisGift = \App\Models\Registrations_gift::first();
        Mail::to('t.anh.1006@gmail.com')
            ->send(new \App\Mail\RegistrationMail($testRegis));
        Mail::to('t.anh.1006@gmail.com')
            ->send(new \App\Mail\RegistrationGiftMail($testRegisGift));
    });

});