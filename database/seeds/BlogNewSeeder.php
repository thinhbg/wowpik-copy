<?php

use Illuminate\Database\Seeder;

class BlogNewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(\App\Models\Blog::class, 200)->create();
    }
}
