<?php

use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $bCategories = [
            ['name' => "Học và chơi", 'color' => '#e65625'],
            ['name' => "Vận động & Phát triển", 'color' => 'rgba(130, 195, 73, 1)'],
            ['name' => "Giác quan & Kỹ năng", 'color' => 'rgb(241, 203, 50)'],
            ['name' => "Nghệ thuật", 'color' => 'rgb(72, 86, 160)'],
            ['name' => "Giáo dục sớm", 'color' => 'rgb(127, 177, 226)'],
            ['name' => "Góc cha mẹ", 'color' => 'rgb(55,70,100)'],
        ];

        $bPeriods = [
            ['name' => '0 - 6 tháng'],
            ['name' => '6 - 9 tháng'],
            ['name' => '9 - 12 tháng'],
            ['name' => '1 - 2 tuổi'],
            ['name' => '2 - 4 tuổi']
        ];

        DB::table('b_categories')->insert($bCategories);
        DB::table('b_periods')->insert($bPeriods);
    }
}