<?php

use Illuminate\Database\Seeder;

class SettingNew extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('settings')->insert([
            'key' => 'service_expert_background',
            'name' => 'Service Expert Background',
            'description' => '',
            'value' => 'assets/images/service/demo-promotion.jpeg',
            'field' => '{"name":"value","label":"Value","type":"browse"}',
            'active' => 1,
            'created_at' => null,
            'updated_at' => null
        ]);
    }
}
