<?php

use Illuminate\Database\Seeder;

class DiyPostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\DiyTheme::class, 20)->create();
    }
}
