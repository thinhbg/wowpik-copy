<?php

use Illuminate\Database\Seeder;

class ProductImages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    protected $_notices = [];

    public function run()
    {
        //
        $tProducts = \App\Models\TProduct::all();

        $imageFolder = public_path('uploads/products');
        $dirs = new DirectoryIterator($imageFolder);

        foreach ($dirs as $fileInfo) {

            //Brand Name
            if ($fileInfo->isDir() && !$fileInfo->isDot() && $fileInfo->getSize() > 0) {

                if($fileInfo->getFilename() == 'WOWPIK') {
                    continue;
                }

                echo $fileInfo->getFilename(). PHP_EOL;
                $subFolder = $imageFolder . '/' . $fileInfo->getFilename();

                $newFolderName = str_replace(' ', '-', $subFolder);
                rename($subFolder,$newFolderName);

                $this->_processBrandFolder($newFolderName);
            }
        }

        print_r($this->_notices);
    }

    function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    protected function _processSkuFolder($kuFolder, $products) {
        $files = new DirectoryIterator($kuFolder);

        $galleries = [];
        $mainImage = null;
        foreach ($files as $fileInfo) {
            //Brand Name
            if($fileInfo->isFile() && in_array($fileInfo->getExtension(), ['jpg', 'png']) ) {
                $name = str_replace('.'. $fileInfo->getExtension(), '', $fileInfo->getFilename());
                if($this->endsWith($name, '1')) {
                    $mainImage = str_replace(public_path(), '', $kuFolder . '/' . $fileInfo->getFilename());
                } else {
                    $galleries [] = str_replace(public_path(), '', $kuFolder . '/' . $fileInfo->getFilename());
                }
            }
        }

        $mainImage = $mainImage ? $mainImage : (count($galleries) ? $galleries[0] : null);

        foreach ($products as $product) {
            $product->image = str_replace('public/', '', $mainImage);
            $product->galleries = json_encode($galleries);
            $product->save();
        }
    }

    protected function _processBrandFolder($fileInfo) {

        $exceptSkus = [
            'LT-628566M','4914', 'BRU02536', 'Touch Learn Activity Desk', 'HAPE', '208201'
        ];

        $dirs = new DirectoryIterator($fileInfo);

        foreach ($dirs as $dir) {
            $skuPart = $dir->getFilename();
            $products = \App\Models\TProduct::where('sku', 'like', "%{$dir}%")->get();

            if ($dir->isDir() && !$dir->isDot() && $dir->getSize() > 0) {

                if(count($products)) {
                    echo $skuPart . ' '. count($products) . PHP_EOL;

                    //Tien hanh import product
                    $skuFolder = $fileInfo . '/' . $skuPart;
                    $this->_processSkuFolder($skuFolder, $products);

                    if(count($products) > 1) {
                        $this->_notices[$skuPart] = count($products);
                        echo "NOTICE" . PHP_EOL;
                    }
                } else {
                    if(in_array($skuPart, $exceptSkus)) {
                        continue;
                    }
                    die($skuPart . ' error' . PHP_EOL) ;
                }
            }
        }
    }
}
