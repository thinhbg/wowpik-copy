<?php

use Illuminate\Database\Seeder;

class DiySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categories = [
            1 => ['color' => '#e65625'],
            2 => ['color' => 'rgb(151,195,90)'],
            3 => ['color' => 'rgb(241,200,50)'],
            4 => ['color' => 'rgb(123,178,226)'],
            5 => ['color' => 'rgb(72, 86 , 161)']
        ];

        foreach ($categories as  $key => $category) {
            $diyCategory = new \App\Models\DiyCategory();
            $diyCategory->fill([
                'title' => 'Chủ đề ' . $key,
                'color' => $category['color'],
            ])->save();
        }
    }
}
