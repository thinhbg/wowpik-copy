<?php

use Illuminate\Database\Seeder;

class ImportProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\TProduct::query()->delete();
        \App\Models\TProductBrand::query()->delete();

        $file = fopen("products.csv","r");

        $line = 0;

        $brands = [];
        $productData = [];
        while(! feof($file)) {
            $data = fgetcsv($file);
            if($line == 0) {
                $line++;
                continue;
            }

            if(trim($data[3])) {
                $brands [] = $data[3];
            }

            $productData [] = $data;
            $line++;
        }

        $brands = array_unique($brands);
        foreach ($brands as $brand) {
            (new \App\Models\TProductBrand())->fill(['name' => $brand])->save();
        }

        $brands = \App\Models\TProductBrand::all()->pluck('id', 'name')->toArray();
        foreach($productData as $productDatum) {

            if(!$productDatum[0]) {
                break;
            }

            $description = [];

            if($productDatum[5]) {
                $description [] = [
                    'desc' => $productDatum[5] .": ".$productDatum[6],
                    'col' => 1
                ];
            }

            if($productDatum[7]) {
                $description [] = [
                    'desc' => $productDatum[7] .": ".$productDatum[8],
                    'col' => 1
                ];
            }

            if($productDatum[9]) {
                $description [] = [
                    'desc' => $productDatum[9] .": ".$productDatum[10],
                    'col' => 1
                ];
            }

//            if($productDatum[7]) {
//                $description [] = [
//                    'desc' => $productDatum[7],
//                    'col' => 1
//                ];
//            }

            (new \App\Models\TProduct())->fill([
                'name' => $productDatum[0],
                'brand_id' => $productDatum[3] ? $brands[$productDatum[3]] : null,
                'description' => json_encode($description),
                'version_name' => $productDatum[11],
                'sku' => $productDatum[12],
                'weight' => (float) $productDatum[14],
                'unit' => $productDatum[17],
                'is_tax' => $productDatum[18] == 'Có' ? 1 : 0,
                'lc_cn1_init_cost' => $productDatum[19],
                'lc_cn1_init_qty' => $productDatum[20],
                'lc_cn1_min_qty' => $productDatum[21],
                'lc_cn1_max_qty' => $productDatum[22],
                'lc_cn1_stock_point' => $productDatum[23],
                'pl_wholesale_price' => $productDatum[24],
                'pl_import_price' => $productDatum[25],
                'price' => $productDatum[26]
            ])->save();
        }

        fclose($file);
    }
}
