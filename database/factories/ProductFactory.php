<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Models\TProduct::class, function (Faker $faker) {
    return [
        'name'        => ucfirst($faker->unique()->sentence()),
        'image' => 'uploads/products/test3.jpg',
        'sku' => $faker->unique()->sentence(),
        'price'       => rand(0, 1000000),
        'type_id' => rand(0,4),
        'material_id' => rand(0,4),
        'age_range_id' => rand(0,5),
        'brand_id' => rand(0,20),
    ];
});