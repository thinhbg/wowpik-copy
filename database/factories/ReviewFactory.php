<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Models\Review::class, function (Faker $faker) {
    $productIds = [604, 605];

    return [
        'user_name' => ucfirst($faker->unique()->sentence()),
        'comment' => ucfirst($faker->unique()->sentence()),
        'title' => $faker->unique()->sentence(),
        'rates' => rand(1,5),
        'product_id' => $productIds[rand(0,1)]
    ];
});