<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DiyPost;
use App\Models\DiyTheme;
use Faker\Generator as Faker;

$factory->define(DiyPost::class, function (Faker $faker) {
    return [
        'title' => ucfirst($faker->unique()->sentence()),
        'image' => 'uploads/blogs/baby.jpg',
        'category_id' => rand(1, 5),
        'content' => $faker->unique()->text(20000),
        'writer' => $faker->unique()->text(1000),
        'video_link' => '',
        'video_guide_link' => 'https://www.youtube.com/embed/oPVte6aMprI',
        'download_link' => 'uploads/blogs/baby.jpg'
    ];
});
