<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Models\Blog::class, function (Faker $faker) {
    return [
        'name' => ucfirst($faker->unique()->sentence()),
        'image' => 'uploads/blogs/baby.jpg',
        'category_id' => rand(0, 6),
        'period_id' => rand(0,5),
        'content' => $faker->unique()->text(20000),
    ];
});