<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DiyTheme;
use Faker\Generator as Faker;

$factory->define(DiyTheme::class, function (Faker $faker) {
    return [
        'title' => ucfirst($faker->unique()->sentence(3)),
        'image' => 'uploads/blogs/baby.jpg',
    ];
});
