<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBlog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //
        Schema::create('related_blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('blog_id');
            $table->integer('related_id');
        });

        Schema::table('blogs', function (Blueprint $table) {
            $table->string('big_banner')->nullable();
            $table->string('video')->nullable();
            $table->longText('content');
            $table->longText('extras')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}