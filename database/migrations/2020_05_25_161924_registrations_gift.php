<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RegistrationsGift extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //
        Schema::create('registrations_gift', function (Blueprint $table) {
            $table->increments('id');

            $table->string('child_name');
            $table->string('child_home_name');
            $table->string('child_dob_year');
            $table->string('child_dob_month');
            $table->string('child_dob_day');

            $table->string('customer_name');
            $table->string('customer_email');
            $table->string('customer_phone');

            $table->string('receiver_name');
            $table->string('receiver_city');
            $table->string('receiver_address');
            $table->string('receiver_state');
            $table->string('receiver_phone');
            $table->string('receiver_phuong')->nullable();
            $table->text('receiver_note');

            $table->string('price_option');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
