<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('t_products', function (Blueprint $table) {
            $table->string('type_code')->nullable();
            $table->integer('brand_id')->nullable()->change();
            $table->integer('country_id')->nullable();
//            $table->string('size')->nullable();
            $table->string('version_name')->nullable();
            $table->float('weight')->default(0);
            $table->string('unit')->nullable();
            $table->tinyInteger('is_tax')->default(0);
            $table->float('lc_cn1_init_cost')->default(0);
            $table->float('lc_cn1_init_qty')->default(0);
            $table->integer('lc_cn1_min_qty')->default(0);
            $table->integer('lc_cn1_max_qty')->default(0);
            $table->string('lc_cn1_stock_point')->default(0);
            $table->float('pl_wholesale_price')->default(0);
            $table->float('pl_import_price')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
