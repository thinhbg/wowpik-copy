<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDiyTheme extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('diy_categories', function (Blueprint $table) {
           $table->integer('theme_id')->nullable();
        });

        Schema::table('diy_themes', function (Blueprint $table) {
            $table->string('slug')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('diy_categories', function (Blueprint $table) {
            $table->removeColumn('theme_id');
        });

        Schema::table('diy_themes', function (Blueprint $table) {
            $table->removeColumn('slug');
        });
    }
}
