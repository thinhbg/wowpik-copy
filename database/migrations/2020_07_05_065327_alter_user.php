<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->integer('is_admin')->default(0);
            $table->string('phone')->nullable();

            $table->string('baby_name')->nullable();
            $table->integer('baby_year')->nullable();
            $table->longText('baby_nicknames')->nullable();
            $table->longText('baby_hobbies')->nullable();
            $table->longText('notes')->nullable();
        });


        Schema::create('user_address',  function (Blueprint $table) {
            $table->integer('id')->index()->primary();
            $table->integer('user_id')->index();
            $table->string('address')->nullable();
            $table->string('address_phuong')->nullable();
            $table->string('address_quan')->nullable();
            $table->string('address_country')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
