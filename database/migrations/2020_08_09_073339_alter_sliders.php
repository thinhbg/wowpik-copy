<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSliders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sliders', function (Blueprint $table) {
            $table->integer('button_display')->default(1);
            $table->string('button_text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('sliders', function (Blueprint $table) {
            $table->removeColumn('button_display');
            $table->removeColumn('button_text');
        });
    }
}
