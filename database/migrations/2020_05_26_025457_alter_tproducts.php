<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTproducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('t_products', function (Blueprint $table) {
            $table->string('slug')->nullable()->unique()->after('name');
            $table->text('galleries')->nullable();
            $table->string('sku')->nullable()->unique();
            $table->longText('description')->nullable();
        });

        Schema::create('t_products_related', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('related_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}