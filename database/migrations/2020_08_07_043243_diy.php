<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Diy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('diy_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->string('title');
            $table->string('image');
            $table->text('slug');
            $table->text('video_link')->nullable();
            $table->longText('content');
            $table->longText('writer')->nullable();
            $table->longText('extras')->nullable();
            $table->text('download_link');
            $table->text('video_guide_link');


            $table->timestamps();
        });

        Schema::create('diy_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('color');
            $table->timestamps();
        });

        Schema::create('diy_post_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->integer('diy_post_id');
            $table->timestamps();
        });

        Schema::create('diy_related_post', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('diy_post_id');
            $table->integer('related_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::dropIfExists('diy_related_post');
        Schema::dropIfExists('diy_post_categories');
        Schema::dropIfExists('diy_categories');
        Schema::dropIfExists('diy_posts');
    }
}
