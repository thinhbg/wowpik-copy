<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegistrationGiftMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $registrationGift;
    public function __construct($registrationGift = null)
    {
        //
        $this->registrationGift = $registrationGift;
        $this->subject('Bạn có "Đăng Ký Quà Tặng" mới ');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('web.mail.registrationGift', [
            'registration' => $this->registrationGift
        ]);
    }
}
