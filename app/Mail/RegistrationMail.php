<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegistrationMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $registration;
    public function __construct($registration = null)
    {
        //
        $this->registration = $registration;
        $this->subject('Bạn có "Đăng Ký Cho Con" mới ');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('web.mail.registration', [
            'registration' => $this->registration
        ]);
    }
}
