<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class ServiceItem extends Model
{
    use CrudTrait;


    CONST TYPE_3STEPS = 1;
    CONST TYPE_REWARDS = 2;
    CONST TYPE_PRESERVATION = 3;
    CONST TYPE_EXPERTS = 4;

    CONST SUB_TYPE_PRESERVATION_1 = 1;
    CONST SUB_TYPE_PRESERVATION_2 = 2;
    CONST SUB_TYPE_PRESERVATION_3 = 3;
    CONST SUB_TYPE_PRESERVATION_4 = 4;

    CONST SUB_TYPE_REWARD_1 = 5;
    CONST SUB_TYPE_REWARD_2 = 6;
    CONST SUB_TYPE_REWARD_3 = 7;


    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'service_items';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public static function loadByType($type,  $subtype = null) {
        $collections = self::all()
            ->where('type', '=', $type);

        return $collections;
    }

    public static function loadBySubType($subType) {
        $collections = self::all()
            ->where('sub_type', '=', $subType);

        return $collections;
    }

    public static function getFrontSubTypes() {
        return [
            ServiceItem::SUB_TYPE_PRESERVATION_1 => 'Bảo quản',
            ServiceItem::SUB_TYPE_PRESERVATION_2 => 'Vệ sinh',
            ServiceItem::SUB_TYPE_PRESERVATION_3 => 'Duy trì',
            ServiceItem::SUB_TYPE_PRESERVATION_4 => 'Nâng cấp',
        ];
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
