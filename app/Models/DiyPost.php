<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class DiyPost extends Model
{
    use CrudTrait;
    use \Cviebrock\EloquentSluggable\Sluggable;
    use \Cviebrock\EloquentSluggable\SluggableScopeHelpers;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'diy_posts';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function relatedDiy() {
        return $this->belongsToMany(\App\Models\DiyPost::class, 'diy_related_post', 'diy_post_id', 'related_id');
    }

    public function category() {
        return $this->belongsTo(DiyCategory::class, 'category_id');
    }

    public function theme() {
        return $this->category->theme;
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_or_title',
            ],
        ];
    }

    // The slug is created automatically from the "name" field if no slug exists.
    public function getSlugOrTitleAttribute()
    {
        if ($this->slug != '') {
            return $this->slug;
        }
        return $this->title . time();
    }
    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function getDownloadLinK() {
        return url('download?diy_id=' . $this->id);
    }

    public function getDiyLink()
    {
        return url('/goc-kheo-tay/'.$this->slug);
    }

    public static function loadBySlug($slug) {
        return self::where('slug', '=', $slug)->first();
    }
}
