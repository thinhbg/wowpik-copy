<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use DB;

class TProduct extends Model
{
    use CrudTrait;

    use \Cviebrock\EloquentSluggable\Sluggable;
    use \Cviebrock\EloquentSluggable\SluggableScopeHelpers;


    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 't_products';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function brand()
    {
        return $this->belongsTo(TProductBrand::class, 'brand_id');
    }

//    public function age()
//    {
//        return $this->belongsTo(TProductAge::class, 'age_range_id');
//    }

    public function ages() {
        return $this->belongsToMany(TProductAge::class, 't_product_ages_rlt', 'product_id','age_range_id');
    }

    public function material()
    {
        return $this->belongsTo(TProductMaterial::class, 'material_id');
    }

    public function type()
    {
        return $this->belongsTo(TProductType::class, 'type_id');
    }

    public function country() {
        return $this->belongsTo(Countries::class, 'country_id');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_or_name',
            ],
        ];
    }

    // The slug is created automatically from the "name" field if no slug exists.
    public function getSlugOrNameAttribute()
    {
        if ($this->slug != '') {
            return $this->slug;
        }
        return $this->name . time();
    }

    public function getProductLink()
    {
        return url('/do-choi/'.$this->slug);
    }

    public static function loadBySlug($slug) {
        return self::where('slug', '=', $slug)->first();
    }

    public function relatedProducts() {
        return $this->belongsToMany(\App\Models\TProduct::class, 't_products_related', 'product_id', 'related_id');
    }

    public function sameBrandProducts() {
        if($this->brand) {
            return self::where('brand_id', '=', $this->brand->id)->paginate(4);
        }

        return null;
    }

    public function reviews() {
        return $this->hasMany(\App\Models\Review::class, 'product_id', 'id');
    }

    public function activeReviews() {
        return $this->hasMany(\App\Models\Review::class, 'product_id', 'id')->where('status', '=', 1);
    }

    public function getRatesAttribute() {
        $result =  DB::table('reviews')
            ->select(DB::raw('AVG(rates) as rates'))
            ->where('product_id', '=', $this->id)
            ->where('status', 1)
            ->get()->toArray();

        if(count($result)) {
            return (int) $result[0]->rates;
        }

        return 0;
    }

    public function getImageLink() {
        if($this->image) {
            return url($this->image);
        }

        return url('uploads/products/place_holder.jpg');
    }

    public static function getFilters() {
        return [
            'types' => [
                'label' => 'Phân loại',
                'key' => 'type_id',
                'menu-class' => 'col-2',
                'options' => TProductType::all()->pluck('name', 'id')->toArray(),
            ],
            'ages' => [
                'label' => 'Độ tuổi',
                'key' => 'age_range_id',
                'menu-class' => 'col-2',
                'options' => TProductAge::all()->pluck('name', 'id')->toArray(),
            ],
            'brands' => [
                'label' => 'Thương hiệu',
                'key' => 'brand_id',
                'menu-class' => 'col d-flex flex-wrap',
                'item-class' => 'w-25 text-truncate',
                'options' => TProductBrand::all()->pluck('name', 'id')->toArray(),
            ],
            'materials' => [
                'label' => 'Chất liệu',
                'key' => 'material_id',
                'menu-class' => 'col-2',
                'options' => TProductMaterial::all()->pluck('name', 'id')->toArray(),
            ],
        ];
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
