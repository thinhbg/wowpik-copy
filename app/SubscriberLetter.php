<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriberLetter extends Model
{
    //

    protected $table = 'subscriber_letters';

    protected $guarded = ['id'];


}
