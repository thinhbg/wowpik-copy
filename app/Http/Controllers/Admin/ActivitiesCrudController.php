<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ActivitiesRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ActivitiesCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ActivitiesCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Activities');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/activities');
        $this->crud->setEntityNameStrings('activities', 'activities');

        $this->crud->addField([
            'name' => 'type',
            'type' => 'select_from_array',
            'options' => [
                1 => 'Image',
                2 => 'Video'
            ]
        ]);

        $this->crud->addField([
            'name' => 'image',
            'type' => 'browse',
        ]);

        $this->crud->addField([
            'name' => 'link',
            'type' => 'text',
            'label' => 'Youtube Link'
        ]);

        $this->crud->addColumn([
            'name' => 'image',
            'type' => 'image',
        ]);

        $this->crud->addColumn([
            'name' => 'type',
            'type' => 'select_from_array',
            'options' => [
                1 => 'Image',
                2 => 'Video'
            ]
        ]);
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ActivitiesRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
