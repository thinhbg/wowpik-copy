<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TProductRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TProductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;


    CONST TAB_INFO = 'Information';
    CONST TAB_MEDIA = 'Media';
    CONST TAB_OPTIONS = 'Options';
    CONST TAB_RELATED = 'Related';
    CONST TAB_STOCK_AND_PRICE = 'Price And Stock';

    public function setup()
    {
        $this->crud->setModel('App\Models\TProduct');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/tproduct');
        $this->crud->setEntityNameStrings('tproduct', 't_products');


        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'tab' => self::TAB_INFO
        ]);

        $this->crud->addField([
            'name' => 'slug',
            'type' => 'text',
            'tab' => self::TAB_INFO
        ]);

        $this->crud->addField([
            'name' => 'description',
            'type' => 'wowpik/table',
            'columns' => [
                'desc' => 'Description',
                'col' => 'Column'
            ],
            'tab' => self::TAB_INFO
        ]);

        $this->crud->addField([
            'name' => 'is_tax',
            'type' => 'boolean',
            'tab' => self::TAB_INFO
        ]);

        $this->crud->addField([
            'name' => 'sku',
            'type' => 'text',
            'tab' => self::TAB_INFO
        ]);

        $this->crud->addField([
            'name' => 'price',
            'type' => 'number',
            'tab' => self::TAB_STOCK_AND_PRICE,
            'default' => 0
        ]);

        $this->crud->addField([
            'name' => 'weight',
            'type' => 'number',
            'tab' => self::TAB_STOCK_AND_PRICE,
            'default' => 0
        ]);

        $this->crud->addField([
            'name' => 'lc_cn1_init_cost',
            'type' => 'number',
            'tab' => self::TAB_STOCK_AND_PRICE,
            'default' => 0
        ]);

        $this->crud->addField([
            'name' => 'lc_cn1_init_qty',
            'type' => 'number',
            'tab' => self::TAB_STOCK_AND_PRICE,
            'default' => 0
        ]);

        $this->crud->addField([
            'name' => 'lc_cn1_min_qty',
            'type' => 'number',
            'tab' => self::TAB_STOCK_AND_PRICE,
            'default' => 0
        ]);

        $this->crud->addField([
            'name' => 'lc_cn1_max_qty',
            'type' => 'number',
            'tab' => self::TAB_STOCK_AND_PRICE,
            'default' => 0
        ]);

        $this->crud->addField([
            'name' => 'lc_cn1_stock_point',
            'type' => 'number',
            'tab' => self::TAB_STOCK_AND_PRICE,
            'default' => 0
        ]);

        $this->crud->addField([
            'name' => 'pl_wholesale_price',
            'type' => 'number',
            'tab' => self::TAB_STOCK_AND_PRICE,
            'default' => 0
        ]);

        $this->crud->addField([
            'name' => 'pl_import_price',
            'type' => 'number',
            'tab' => self::TAB_STOCK_AND_PRICE,
            'default' => 0
        ]);

        $this->crud->addField([
            // 1-n relationship
            'label' => "Brand", // Table column heading
            'type' => "select",
            'name' => 'brand_id', // the column that contains the ID of that connected entity;
            'entity' => 'brand', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => \App\Models\TProductBrand::class,
            'tab' => self::TAB_OPTIONS
        ]);

        $this->crud->addField([
            // 1-n relationship
            'label' => "Country", // Table column heading
            'type' => "select",
            'name' => 'country_id', // the column that contains the ID of that connected entity;
            'entity' => 'country', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => \App\Models\Countries::class,
//            'tab' => self::TAB_OPTIONS
        ]);

        $this->crud->addField([
            // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Ages",
            'type' => 'select2_multiple',
            'name' => 'ages', // the method that defines the relationship in your Model
            'entity' => 'ages', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => \App\Models\TProductAge::class, // foreign key model
            'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
        ]);

        $this->crud->addField([
            // 1-n relationship
            'label' => "Type", // Table column heading
            'type' => "select",
            'name' => 'type_id', // the column that contains the ID of that connected entity;
            'entity' => 'type', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => \App\Models\TProductType::class,
            'tab' => self::TAB_OPTIONS
        ]);

        $this->crud->addField([
            // 1-n relationship
            'label' => "Material", // Table column heading
            'type' => "select",
            'name' => 'material_id', // the column that contains the ID of that connected entity;
            'entity' => 'material', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => \App\Models\TProductMaterial::class,
            'tab' => self::TAB_OPTIONS
        ]);

        $this->crud->addField([
            'name' => 'image',
            'type' => 'browse',
            'tab' => self::TAB_MEDIA
        ]);

        $this->crud->addField([
            'name' => 'galleries',
            'type' => 'browse_multiple',
            'sortable' => true,
            'multiple' => true,
            'tab' => self::TAB_MEDIA
        ]);

        $this->crud->addField([
            'name' => 'video',
            'type' => 'browse',
            'tab' => self::TAB_MEDIA
        ]);



        $this->crud->addField([
            // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Related Products",
            'type' => 'select2_multiple',
            'name' => 'relatedProducts', // the method that defines the relationship in your Model
            'entity' => 'relatedProducts', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => \App\Models\TProduct::class, // foreign key model
            'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?
            'tab' => self::TAB_RELATED
            // optional
            //'options'   => (function ($query) {
            //    return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
            //}), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);

        $this->crud->addField([
            'name' => 'price',
            'type' => 'number'
        ]);

        $this->crud->addColumn([
            'name' => 'image',
            'type' => 'image'
        ]);

        $this->crud->addColumn([
            // 1-n relationship
            'label' => "Brand", // Table column heading
            'type' => "select",
            'name' => 'brand_id', // the column that contains the ID of that connected entity;
            'entity' => 'brand', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => \App\Models\TProductBrand::class,
        ]);

        $this->crud->addColumn([
            // 1-n relationship
            'label' => "Ages",
            'type' => 'select_multiple',
            'name' => 'ages', // the method that defines the relationship in your Model
            'entity' => 'ages', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => \App\Models\TProductAge::class, // foreign key model
        ]);

        $this->crud->addColumn([
            // 1-n relationship
            'label' => "Type", // Table column heading
            'type' => "select",
            'name' => 'type_id', // the column that contains the ID of that connected entity;
            'entity' => 'type', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => \App\Models\TProductType::class,
        ]);

        $this->crud->addColumn([
            // 1-n relationship
            'label' => "Material", // Table column heading
            'type' => "select",
            'name' => 'material_id', // the column that contains the ID of that connected entity;
            'entity' => 'material', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => \App\Models\TProductMaterial::class,
        ]);
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(TProductRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
