<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BrandsRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class BrandsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class BrandsCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Brands');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/brands');
        $this->crud->setEntityNameStrings('brands', 'brands');


        $this->crud->addField([
            'name' => 'image',
            'type' => 'browse'
        ]);

        $this->crud->addField([
            // 1-n relationship
            'label' => "Brand", // Table column heading
            'type' => "select2",
            'name' => 'brand_id', // the column that contains the ID of that connected entity;
            'entity' => 'brand', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => \App\Models\TProductBrand::class,
        ]);

        $this->crud->addColumn([
            'name' => 'image',
            'type' => 'image'
        ]);

        $this->crud->addColumn([
            // 1-n relationship
            'label' => "Brand", // Table column heading
            'type' => "select",
            'name' => 'brand_id', // the column that contains the ID of that connected entity;
            'entity' => 'brand', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => \App\Models\TProductBrand::class,
        ]);
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();


    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(BrandsRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
