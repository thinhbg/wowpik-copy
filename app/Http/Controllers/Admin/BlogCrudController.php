<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BlogRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class BlogCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class BlogCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    CONST INFO_TAB = "Info";
    CONST EXTRA_TAB = "Extra";

    public function setup()
    {
        $this->crud->setModel('App\Models\Blog');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/blog');
        $this->crud->setEntityNameStrings('blog', 'blogs');

        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'tab' => self::INFO_TAB
        ]);

        $this->crud->addField([
            'name' => 'slug',
            'type' => 'text',
            'tab' => self::INFO_TAB
        ]);

        $this->crud->addField([
            'name' => 'image',
            'type' => 'browse',
            'tab' => self::INFO_TAB
        ]);

        $this->crud->addField([
            'name' => 'big_banner',
            'type' => 'browse',
            'tab' => self::INFO_TAB
        ]);

        $this->crud->addField([
            'name' => 'video',
            'type' => 'browse',
            'tab' => self::INFO_TAB
        ]);

        $this->crud->addField([
            'name' => 'content',
            'type' => 'wysiwyg',
            'tab' => self::INFO_TAB
        ]);

        $this->crud->addField([
            'name'     => 'meta_title',
            'label'    => 'Meta Title',
            'fake'     => true,
            'store_in' => 'extras',
            'tab' => self::EXTRA_TAB
        ]);
        $this->crud->addField([
            'name'     => 'meta_description',
            'label'    => 'Meta Description',
            'fake'     => true,
            'store_in' => 'extras',
            'tab' => self::EXTRA_TAB
        ]);
        $this->crud->addField([
            'name'     => 'meta_keywords',
            'type'     => 'textarea',
            'label'    => 'Meta Keywords',
            'fake'     => true,
            'store_in' => 'extras',
            'tab' => self::EXTRA_TAB
        ]);

        $this->crud->addField([
            // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Related Blogs",
            'type' => 'select2_multiple',
            'name' => 'relatedBlogs', // the method that defines the relationship in your Model
            'entity' => 'relatedBlogs', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => \App\Models\Blog::class, // foreign key model
            'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
            'tab' => self::INFO_TAB
        ]);

        $this->crud->addField([
            // 1-n relationship
            'label' => "Category", // Table column heading
            'type' => "select",
            'name' => 'category_id', // the column that contains the ID of that connected entity;
            'entity' => 'category', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => \App\Models\BCategory::class,
            'tab' => self::INFO_TAB
        ]);

        $this->crud->addField([
            // 1-n relationship
            'label' => "Period", // Table column heading
            'type' => "select",
            'name' => 'period_id', // the column that contains the ID of that connected entity;
            'entity' => 'period', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => \App\Models\BPeriod::class,
            'tab' => self::INFO_TAB
        ]);


        $this->crud->addColumn([
            'name' => 'image',
            'type' => 'image'
        ]);

        $this->crud->addColumn([
            // 1-n relationship
            'label' => "Category", // Table column heading
            'type' => "select",
            'name' => 'category_id', // the column that contains the ID of that connected entity;
            'entity' => 'category', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => \App\Models\BCategory::class,
        ]);

        $this->crud->addColumn([
            // 1-n relationship
            'label' => "Period", // Table column heading
            'type' => "select",
            'name' => 'period_id', // the column that contains the ID of that connected entity;
            'entity' => 'period', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => \App\Models\BPeriod::class,
        ]);
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(BlogRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();


        $this->crud->removeField('extras');
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();


        $this->crud->removeField('extras');
    }
}
