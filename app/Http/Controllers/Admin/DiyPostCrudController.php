<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DiyPostRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class DiyPostCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DiyPostCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    CONST INFO_TAB = "Info";
    CONST EXTRA_TAB = "Extra";
    CONST MEDIA_TAB = "Media";

    public function setup()
    {
        $this->crud->setModel('App\Models\DiyPost');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/diypost');
        $this->crud->setEntityNameStrings('diypost', 'diy_posts');

        $this->crud->addField([
            'name' => 'is_free',
            'tab' => self::INFO_TAB
        ]);
        $this->crud->addField([
            'name' => 'title',
            'type' => 'text',
            'tab' => self::INFO_TAB
        ]);

        $this->crud->addField([
            'name' => 'slug',
            'type' => 'text',
            'tab' => self::INFO_TAB
        ]);

        $this->crud->addField([
            'name' => 'image',
            'type' => 'browse',
            'tab' => self::INFO_TAB
        ]);

        $this->crud->addField([
            'name' => 'video_link',
            'type' => 'browse',
            'tab' => self::INFO_TAB
        ]);

        $this->crud->addField([
            'name' => 'download_link',
            'type' => 'browse',
            'tab' => self::MEDIA_TAB
        ]);
        $this->crud->addField([
            'name' => 'video_guide_link',
            'type' => 'text',
            'tab' => self::MEDIA_TAB
        ]);

        $this->crud->addField([
            'name' => 'content',
            'type' => 'wysiwyg',
            'tab' => self::INFO_TAB
        ]);
        $this->crud->addField([
            'name' => 'writer',
            'type' => 'wysiwyg',
            'tab' => self::INFO_TAB
        ]);

        $this->crud->addField([
            'name'     => 'meta_title',
            'label'    => 'Meta Title',
            'fake'     => true,
            'store_in' => 'extras',
            'tab' => self::EXTRA_TAB
        ]);
        $this->crud->addField([
            'name'     => 'meta_description',
            'label'    => 'Meta Description',
            'fake'     => true,
            'store_in' => 'extras',
            'tab' => self::EXTRA_TAB
        ]);
        $this->crud->addField([
            'name'     => 'meta_keywords',
            'type'     => 'textarea',
            'label'    => 'Meta Keywords',
            'fake'     => true,
            'store_in' => 'extras',
            'tab' => self::EXTRA_TAB
        ]);

        $this->crud->addField([
            // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Related Diy",
            'type' => 'select2_multiple',
            'name' => 'relatedDiy', // the method that defines the relationship in your Model
            'entity' => 'relatedDiy', // the method that defines the relationship in your Model
            'attribute' => 'title', // foreign key attribute that is shown to user
            'model' => \App\Models\DiyPost::class, // foreign key model
            'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
            'tab' => self::INFO_TAB
        ]);

        $this->crud->addField([
            // 1-n relationship
            'label' => "Category", // Table column heading
            'type' => "select",
            'name' => 'category_id', // the column that contains the ID of that connected entity;
            'entity' => 'category', // the method that defines the relationship in your Model
            'attribute' => "title", // foreign key attribute that is shown to user
            'model' => \App\Models\DiyCategory::class,
            'tab' => self::INFO_TAB
        ]);

        $this->crud->addColumn([
            'name' => 'image',
            'type' => 'image'
        ]);

        $this->crud->addColumn([
            // 1-n relationship
            'label' => "Category", // Table column heading
            'type' => "select",
            'name' => 'category_id', // the column that contains the ID of that connected entity;
            'entity' => 'category', // the method that defines the relationship in your Model
            'attribute' => "title", // foreign key attribute that is shown to user
            'model' => \App\Models\DiyCategory::class,
        ]);

    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
//        $this->crud->removeField('slug');
        $this->crud->removeField('extras');
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(DiyPostRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
//        $this->crud->removeField('slug');
        $this->crud->removeField('extras');
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
//        $this->crud->removeField('slug');
        $this->crud->removeField('extras');
    }
}
