<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DiyCategoryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class DiyCategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DiyCategoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\DiyCategory');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/diycategory');
        $this->crud->setEntityNameStrings('diycategory', 'diy_categories');

        $this->crud->addField([
            // 1-n relationship
            'label' => "Theme", // Table column heading
            'type' => "select",
            'name' => 'theme_id', // the column that contains the ID of that connected entity;
            'entity' => 'Theme', // the method that defines the relationship in your Model
            'attribute' => "title", // foreign key attribute that is shown to user
            'model' => \App\Models\DiyTheme::class,
        ]);

        $this->crud->addColumn([
            // 1-n relationship
            'label' => "Theme", // Table column heading
            'type' => "select",
            'name' => 'theme_id', // the column that contains the ID of that connected entity;
            'entity' => 'Theme', // the method that defines the relationship in your Model
            'attribute' => "title", // foreign key attribute that is shown to user
            'model' => \App\Models\DiyTheme::class,
        ]);
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(DiyCategoryRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
