<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ServiceItemRequest;
use App\Models\ServiceItem;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ServiceItemCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ServiceItemCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;



    public function setup()
    {
        $this->crud->setModel('App\Models\ServiceItem');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/serviceitem');
        $this->crud->setEntityNameStrings('Item', 'Service Items');

        $this->crud->addField([
            'name' => 'type',
            'type' => 'select_from_array',
            'options' => [
                ServiceItem::TYPE_3STEPS => '3 Steps',
                ServiceItem::TYPE_REWARDS => 'Rewards',
                ServiceItem::TYPE_PRESERVATION => 'Preservation',
                ServiceItem::TYPE_EXPERTS => 'Experts'
            ]
        ]);

        $this->crud->addField([
            'name' => 'sub_type',
            'type' => 'select_from_array',
            'options' => [
                null => '-',
                ServiceItem::SUB_TYPE_PRESERVATION_1 => 'Bảo hành - Bảo quản',
                ServiceItem::SUB_TYPE_PRESERVATION_2 => 'Bảo hành - Vệ sinh',
                ServiceItem::SUB_TYPE_PRESERVATION_3 => 'Bảo hành - Duy trì',
                ServiceItem::SUB_TYPE_PRESERVATION_4 => 'Bảo hành - Nâng cấp',

                ServiceItem::SUB_TYPE_REWARD_1 => 'Rewards - 1',
                ServiceItem::SUB_TYPE_REWARD_2 => 'Rewards - 2',
                ServiceItem::SUB_TYPE_REWARD_3 => 'Rewards - 3',
            ]
        ]);

        $this->crud->addField([
            'name' => 'image',
            'type' => 'browse',
        ]);

        $this->crud->addColumn([
            'name' => 'image',
            'type' => 'image',
        ]);

        $this->crud->addColumn([
            'name' => 'type',
            'type' => 'select_from_array',
            'options' => [
                ServiceItem::TYPE_3STEPS => '3 Steps',
                ServiceItem::TYPE_REWARDS => 'Rewards',
                ServiceItem::TYPE_PRESERVATION => 'Preservation',
                ServiceItem::TYPE_EXPERTS => 'Experts'
            ]
        ]);
        $this->crud->addColumn([
            'name' => 'sub_type',
            'type' => 'select_from_array',
            'options' => [
                null => '-',
                ServiceItem::SUB_TYPE_PRESERVATION_1 => 'Bảo hành - Bảo quản',
                ServiceItem::SUB_TYPE_PRESERVATION_2 => 'Bảo hành - Vệ sinh',
                ServiceItem::SUB_TYPE_PRESERVATION_3 => 'Bảo hành - Duy trì',
                ServiceItem::SUB_TYPE_PRESERVATION_4 => 'Bảo hành - Nâng cấp',

                ServiceItem::SUB_TYPE_REWARD_1 => 'Rewards - 1',
                ServiceItem::SUB_TYPE_REWARD_2 => 'Rewards - 2',
                ServiceItem::SUB_TYPE_REWARD_3 => 'Rewards - 3',
            ]
        ]);
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ServiceItemRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
