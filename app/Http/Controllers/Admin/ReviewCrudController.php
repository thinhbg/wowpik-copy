<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReviewRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ReviewCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ReviewCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Review');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/review');
        $this->crud->setEntityNameStrings('review', 'reviews');

        $this->crud->addField([  // Select2
            'label' => "Product",
            'type' => 'select2',
            'name' => 'product_id', // the db column for the foreign key
            'entity' => 'product', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            // optional
            'model' => \App\Models\TProduct::class, // foreign key model
        ]);

        $this->crud->addField([
            'label' => 'Rates',
            'name' => 'rates',
            'type' => 'number',
            'max' => 5,
            'min' => 1
        ]);

        $this->crud->addField([
            'label' => 'Status',
            'name' => 'status',
            'type' => 'select_from_array',
            'options' => [
                0 => 'Pending',
                1 => 'Active',
            ]
        ]);

        $this->crud->addColumn([  // Select2
            'label' => "Product",
            'type' => 'select',
            'name' => 'product_id', // the db column for the foreign key
            'entity' => 'product', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            // optional
            'model' => \App\Models\TProduct::class, // foreign key model
        ]);

//        $this->crud->addColumn([  // Select2
//            'label' => "Product Image",
//            'name' => 'product_image',
//            'type' => 'model_function',
//            'function_name' => 'getProductImage'
//        ]);
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ReviewRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();

        $this->crud->removeField('user_id');
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

        $this->crud->removeField('user_id');
    }
}
