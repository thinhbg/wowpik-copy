<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ToysRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ToysCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ToysCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Toys');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/toys');
        $this->crud->setEntityNameStrings('toys', 'toys');

        $this->crud->addField([
            'name' => 'image',
            'type' => 'browse'
        ]);

        $this->crud->addField([
            // 1-n relationship
            'label' => "Product", // Table column heading
            'type' => "select2",
            'name' => 'product_id', // the column that contains the ID of that connected entity;
            'entity' => 'product', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => \App\Models\TProduct::class,
        ]);

        $this->crud->addColumn([
            'name' => 'image',
            'type' => 'image'
        ]);

        $this->crud->addColumn([
            // 1-n relationship
            'label' => "Product", // Table column heading
            'type' => "select",
            'name' => 'product_id', // the column that contains the ID of that connected entity;
            'entity' => 'product', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => \App\Models\TProduct::class,
        ]);
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ToysRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
