<?php

namespace App\Http\Controllers\Web;

use App\Models\Slidergroup;
use App\Models\TProduct;
use App\Models\TProductAge;
use App\Models\TProductBrand;
use App\Models\TProductMaterial;
use App\Models\TProductType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;

class ProductController extends Controller
{
    CONST PER_PAGE = 18;
    //
    public function index() {
        $data = [
            'title' => 'Đồ chơi',
            'filters' => TProduct::getFilters(),
            'slider' => \App\Models\Brands::all()->sortBy('order'),
        ];

        if($type = \request()->get('type_id')) {
            $products = TProduct::where('type_id', '=', $type)
                ->orderBy('order', 'DESC');
        }

        if($age = \request()->get('age_range_id')) {
            $products = TProduct::whereHas('ages', function ($query) use ($age) {
                return $query->where('age_range_id', '=', $age);
            });
        }

        if($brand = \request()->get('brand_id')) {
            $products = TProduct::where('brand_id', '=', $brand)
                ->orderBy('order', 'DESC');
        }

        if($material = \request()->get('material_id')) {
            $products = TProduct::where('material_id', '=', $material)
                ->orderBy('order', 'DESC');
        }

        if($country = \request()->get('country_id')) {
            $products = TProduct::where('country_id', '=', $country)
                ->orderBy('order', 'DESC');
        }

        if(!isset($products)) {
            $products = TProduct::orderBy('order', 'DESC');
        }

        $q = \request()->get('q');
        if(trim($q)) {
            $products->where('name', 'like', "%{$q}%")
                ->orWhere(function ($query) use($q)  {
                    $query->whereHas('brand', function (QueryBuilder $query) use($q) {
                        $query->where('name', 'like', "%{$q}%");
                    });
                });
        }

        $data['products'] = $products
            ->paginate(self::PER_PAGE)->appends($_GET);

        return view('web.products', $data);
    }

    public function show($slug) {
        $product = TProduct::loadBySlug($slug);

        if($product) {
            $data = [
              'product' => $product
            ];
           return view('web.product.show', $data);
        }

        abort(404);
    }
}
