<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;

class RegisterController extends Controller
{
    //
    public function submitRegister() {
        try {

            $params = \request()->all();

            unset($params['_token']);
            $params['price_option'] = $params['price-option'];
            unset($params['price-option']);

            $registration = new \App\Models\Registrations();
            $registration->fill($params)->save();

            $registration->sendNotifyMail();

            return [
                'status' => 200,
                'data' => [
                    'model' => $registration
                ]
            ];

        } catch (\Exception $exception) {
            return [
                'status' => 500,
                'data' => [
                    'msg' => $exception->getMessage()
                ]
            ];
        }
    }

    public function submitGift() {
        try {

            $params = \request()->all();

            unset($params['_token']);
            $params['price_option'] = $params['price-option'];
            unset($params['price-option']);

            $registration = new \App\Models\Registrations_gift();
            $registration->fill($params)->save();

            $registration->sendNotifyMail();

            return [
                'status' => 200,
                'data' => [
                    'model' => $registration
                ]
            ];

        } catch (\Exception $exception) {
            return [
                'status' => 500,
                'data' => [
                    'msg' => $exception->getMessage()
                ]
            ];
        }
    }
}
