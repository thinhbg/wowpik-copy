<?php

namespace App\Http\Controllers\Web;

use App\Models\ServiceItem;
use App\Models\Slidergroup;
use App\User;
use Backpack\PageManager\app\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Auth;
use Hash;

class IndexController extends Controller
{

    public function index() {

        $data = [
            'title' => 'Trang chủ',
            'slider' => Slidergroup::loadByCode('home_slider'),
            'toys' => \App\Models\Toys::all()->sortBy('order'),
            'brands' => \App\Models\Brands::all()->sortBy('order'),
            'countries' => \App\Models\Countries::all()->sortBy('order'),
            'testimonial' => Slidergroup::loadByCode('customer_testimonial'),
            'activities' => \App\Models\Activities::all()->sortBy('order'),
        ];

        $activities = \App\Models\Activities::all()->sortBy('order');
        $rows = [];
        if(count($activities)) {

            $size = 0;
            $rowCount = 1;
            foreach ($activities as $activity) {
                $colSize = $activity->type == 1 ? 1 : 2;

                if(!isset($rows[$rowCount])) {
                    $rows[$rowCount] = [];
                }

                if($size + $colSize <= 3) {
                    $rows[$rowCount] [] = $activity;
                    $size += $colSize;
                } else {
                    $rowCount++;
                    $rows[$rowCount] [] = $activity;
                    $size = 0;
                    $size += $colSize;
                }
            }
        }

        $data['rows'] = $rows;

        return view('web.home', $data);
    }

    public function service() {

        $data = [
            'title' => 'Dịch vụ',
            'slider' => Slidergroup::loadByCode('service_slider'),
            'steps_items' => ServiceItem::loadByType(\App\Models\ServiceItem::TYPE_3STEPS),
            'reward_items' => ServiceItem::loadByType(\App\Models\ServiceItem::TYPE_REWARDS),
            'experts_items' => ServiceItem::loadByType(\App\Models\ServiceItem::TYPE_EXPERTS),
            'preservation_items' => [
                ServiceItem::SUB_TYPE_PRESERVATION_1 => ServiceItem::loadBySubType(ServiceItem::SUB_TYPE_PRESERVATION_1),
                ServiceItem::SUB_TYPE_PRESERVATION_2 => ServiceItem::loadBySubType(ServiceItem::SUB_TYPE_PRESERVATION_2),
                ServiceItem::SUB_TYPE_PRESERVATION_3 => ServiceItem::loadBySubType(ServiceItem::SUB_TYPE_PRESERVATION_3),
                ServiceItem::SUB_TYPE_PRESERVATION_4 => ServiceItem::loadBySubType(ServiceItem::SUB_TYPE_PRESERVATION_4),
            ],
            'option' => \request()->get('option')
        ];

        return view('web.service', $data);
    }


    public function search() {
        $type = request()->get('loai');

        if($type == 'blog') {
            return redirect(route('goc-chia-se') . '?q=' . \request()->get('q'));
        } else {
            return redirect(route('do-choi') . '?q=' . \request()->get('q'));
        }
    }

    public function tintuc($slug) {
        $page = Page::findBySlug($slug);
        if(!$page) {
            abort(404);
        }

        return view('web.new', ['page' => $page]);
    }
}