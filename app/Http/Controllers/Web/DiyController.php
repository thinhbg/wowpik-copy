<?php

namespace App\Http\Controllers\Web;

use App\DiyDownloadHistory;
use App\Models\DiyCategory;
use App\Models\Blog;
use App\Models\BPeriod;
use App\Models\DiyPost;
use App\Models\DiyTheme;
use App\Models\Registrations;
use App\Models\Registrations_gift;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class DiyController extends Controller
{
    //

    public function theme() {
        $themes = DiyTheme::all();

        return view('web.diy-theme', [
            'themes' => $themes
        ]);
    }

    public function index($slug) {


        $theme = DiyTheme::findBySlug($slug);
        if(!$theme) {
            abort(404);
        }

        $data = [
            'categories' => $theme->categories,
        ];

        if($categoryId = \request()->get('category_id')) {
            $blogs = $theme->posts()->where('category_id', '=', $categoryId)
                ->orderBy('created_at', 'DESC');
        }

        if($month = \request()->get('month')) {
            $blogs = $theme->posts()->where(DB::raw('MONTH(diy_posts.created_at)'), '=', $month)
                ->orderBy('created_at', 'DESC');
        }

        if(!isset($blogs)) {
            $blogs = $theme->posts()->orderBy('created_at', 'DESC');
        }

        $q = \request()->get('q');
        if(trim($q)) {
            $blogs->where('name', 'like', "%{$q}%");
        }

        $blogs = $blogs->paginate(7)->appends($_GET);

        $rows = [];
        if(count($blogs)) {
            $size = 0;
            $rowCount = 1;
            $i = 0;

            foreach($blogs as $item) {
                $blogType = 1; //small
                if($i == 0 || $i == 6) {
                    $blogType = 2; //big
                }

                if(!isset($rows[$rowCount])) {
                    $rows[$rowCount] = [];
                }
                $item->type = $blogType;
                if($size + $blogType <= 3) {
                    $rows[$rowCount] [] = $item;
                    $size += $blogType;
                } else {
                    $rowCount++;
                    $rows[$rowCount] [] = $item;
                    $size = 0;
                    $size += $blogType;
                }

                $i++;
            }
        }

        $data['blogs'] = $blogs;
        $data['theme'] = $theme;
        $data['rows'] = $rows;
        return view('web.diys', $data);
    }

    public function show($slug) {

        $diy = DiyPost::loadBySlug($slug);

        if(!$diy) {
            abort(404);
        }

        $data['diy'] = $diy;
        $data['categories'] = $diy->theme()->categories;

        return view('web.diy.show', $data);
    }

    public function download() {

        $id = request()->get('diy_id');
        $diy = DiyPost::find($id);

        if(!$diy) {
            abort(404);
        }

        if($diy->download_link && file_exists(public_path($diy->download_link)) && $this->_canDownload($diy)) {
            $log = new DiyDownloadHistory();
            $log->fill([
                'diy_post_id' => $diy->id,
                'customer_id' => auth()->user()->id,
                'download_link' => $diy->download_link,
                'is_free' => $diy->is_free
            ])->save();

            return response()->download(public_path($diy->download_link));
        }

        abort(403);
    }

    protected function _canDownload($diy) {

        $user = auth()->user();

        if(!$user) {
            return false;
        }

        $email = $user->email;
        $registrations = Registrations::where('customer_email', '=', $email)->get();
        $registrationGifts = Registrations_gift::where('customer_email', '=', $email)->get();

        $quota = [
            'free' => 0,
            'not-free' => 0,
        ];

        foreach ($registrations as $registration) {
            switch ($registration->price_option) {
                case 'yay': {
                    $quota['free'] += 5;
                    break;
                }
                case 'yeah': {
                    $quota['free'] += 5;
                    $quota['not-free'] += 10;
                    break;
                }
                case 'wow': {
                    return true;
                }
            }
        }

        foreach ($registrationGifts as $registration) {
            switch ($registration->price_option) {
                case 'yay': {
                    $quota['free'] += 5;
                    break;
                }
                case 'yeah': {
                    $quota['free'] += 5;
                    $quota['not-free'] += 10;
                    break;
                }
                case 'wow': {
                    return true;
                }
            }
        }

        $isFree = $diy->is_free;

        $downloadHistoryNumber = DB::table('diy_download_history')->select('customer_id', 'diy_post_id', DB::raw('COUNT(*) AS download_number'))
            ->where('is_free', '=', $isFree)
            ->where('customer_id', '=', $user->id)
            ->groupBy('customer_id', 'diy_post_id')
            ->get()->pluck('download_number','diy_post_id')->toArray();

        if($isFree) {
            if(isset($downloadHistoryNumber[$diy->id])) {
                unset($downloadHistoryNumber[$diy->id]);
                $quota['free']--;
            }

            $numberOfPostDownloaded = count($downloadHistoryNumber);
            return $numberOfPostDownloaded < $quota['free'];
        }

        if(isset($downloadHistoryNumber[$diy->id])) {
            unset($downloadHistoryNumber[$diy->id]);
            $quota['not-free']--;
        }

        $numberOfPostDownloaded = count($downloadHistoryNumber);
        return $numberOfPostDownloaded < $quota['not-free'];
    }
}
