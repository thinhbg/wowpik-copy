<?php

namespace App\Http\Controllers\Web;

use App\Models\BCategory;
use App\Models\Blog;
use App\Models\BPeriod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class BlogController extends Controller
{
    //
    public function index() {

        $data = [
            'categories' => BCategory::all(),
            'periods' => BPeriod::all()
        ];

        if($categoryId = \request()->get('category_id')) {
            $blogs = Blog::where('category_id', '=', $categoryId)
                ->orderBy('created_at', 'DESC');
        }

        if($periodId = \request()->get('period_id')) {
            $blogs = Blog::where('period_id', '=', $periodId)
                ->orderBy('created_at', 'DESC');
        }

        if($month = \request()->get('month')) {
            $blogs = Blog::where(DB::raw('MONTH(created_at)'), '=', $month)
                ->orderBy('created_at', 'DESC');
        }

        if(!isset($blogs)) {
            $blogs = Blog::orderBy('created_at', 'DESC');
        }

        $q = \request()->get('q');
        if(trim($q)) {
            $blogs->where('name', 'like', "%{$q}%");
        }

        $blogs = $blogs->paginate(7)->appends($_GET);

        $rows = [];
        if(count($blogs)) {
            $size = 0;
            $rowCount = 1;
            $i = 0;

            foreach($blogs as $item) {
                $blogType = 1; //small
                if($i == 0 || $i == 6) {
                    $blogType = 2; //big
                }

                if(!isset($rows[$rowCount])) {
                    $rows[$rowCount] = [];
                }
                $item->type = $blogType;
                if($size + $blogType <= 3) {
                    $rows[$rowCount] [] = $item;
                    $size += $blogType;
                } else {
                    $rowCount++;
                    $rows[$rowCount] [] = $item;
                    $size = 0;
                    $size += $blogType;
                }

                $i++;
            }
        }

        $data['blogs'] = $blogs;
        $data['rows'] = $rows;
        return view('web.blogs', $data);
    }

    public function show($slug) {

        $data = [
            'categories' => BCategory::all(),
            'periods' => BPeriod::all()
        ];
        $blog = Blog::loadBySlug($slug);

        if(!$blog) {
            abort(404);
        }

        $data['blog'] = $blog;

        return view('web.blog.show', $data);
    }
}
