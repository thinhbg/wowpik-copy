<?php

namespace App\Http\Controllers\Web;

use App\Models\ServiceItem;
use App\Models\Slidergroup;
use App\SubscriberLetter;
use App\User;
use Backpack\PageManager\app\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Auth;
use Hash;

class CustomerController extends Controller
{
    public function dangky() {

        if (auth()->user()) {
            return redirect('trang-chu');
        }
        return view('web.account.register');
    }


    public function theodoi() {

        $refer = \request()->post('referer');
        $email = \request()->post('email');
        try {

            $new = new SubscriberLetter();
            $new->fill([
                'email' => $email
            ])->save();


            return redirect($refer)->with('success', 'Đăng ký theo dõi thành công.');

        } catch (\Exception $exception) {
            return redirect($refer)->with('error', 'Đăng ký thất bại. Vui lòng kiểm tra thông tin.');
        }
    }

    public function dangnhap() {

        if (auth()->user()) {
            return redirect('trang-chu');
        }

        return view('web.account.login');
    }

    public function dangxuat() {

        request()->session()->invalidate();
        request()->session()->regenerateToken();

        return redirect('/');
    }

    public function dangnhapPost() {
        try {
            $data = \request()->all();

            auth()->attempt([
                'email' => $data['email'],
                'password' => $data['password'],
            ]);

            if(!auth()->user()) {
                throw new \Exception('Đăng nhập thất bại.');
            }

            return redirect('trang-chu')->with('success', 'Đăng nhập thành công.');
        } catch (\Exception $exception) {

            return redirect('dang-nhap')->with('error', 'Đăng nhập thất bại. Vui lòng kiểm tra email và mật khẩu');
        }
    }

    public function dangkyPost() {
        try {
            $data = \request()->all();

            $user = User::create([
                'name' => $data['email'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);

            if (!$user->id) {
                throw new \Exception("Đăng ký thất bại");
            }

            auth()->loginUsingId($user->id);

            return redirect('trang-chu')->with('success', 'Đăng ký thành công.');
        } catch (\Exception $exception) {

            return redirect('dang-ky')->with('error', 'Đăng ký không thành công. Vui lòng kiểm tra email');
        }
    }

    public function thongtin() {
        try {
            return view('web.account.info' , ['user' => auth()->user()]);

        } catch (\Exception $exception) {

            return redirect('trang-chu');
        }
    }

    public function thongtinPost() {
        try {

            $params = \request()->post();

            $user = auth()->user();

            $user->fill([
                'email' => $params['email'],
                'name' => $params['name'],
                'phone' => $params['phone'],
                'baby_name' => $params['baby_name'],
                'baby_year' => $params['baby_year'],
                'baby_hobbies' => $params['baby_hobbies'],
                'baby_nicknames' => json_encode($params['nick_names']),
                'notes' => $params['notes'],
            ])->save();

            return redirect('thong-tin')->with('success', 'Lưu thông tin thành công');

        } catch (\Exception $exception) {
            return redirect('thong-tin')->with('error', 'Lưu thông tin không thành công');
        }
    }

    public function diachiPost() {
        try {

            $params = \request()->post();
            $user = auth()->user();


            return redirect('thong-tin')->with('success', 'Lưu thông tin thành công');

        } catch (\Exception $exception) {
            return redirect('thong-tin')->with('error', 'Lưu thông tin không thành công');
        }
    }
}