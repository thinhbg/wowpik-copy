@extends('web.layout.1-column')

@section('title', $product->name)

@section('customs')
<link rel="stylesheet" href="{{ url('css/product.css') }}" />
<link rel="stylesheet" href="{{ url('css/product-detail.css') }}" />
@endsection

@section('content')
<!-- Text freeship -->
<div class="green-background py-1 mb-0 mb-lg-5 text-left">
  <h3 class="freeship-font-responsive text-white text-center mb-0">
    Miễn phí vận chuyển nội thành Hà Nội
  </h3>
</div>
<!-- Giới thệu -->
<p style="width: 95%; margin: auto;">
  <a class="text-decoration-none text-dark" href="{{ url('/trang-chu') }}">Trang chủ</a> >
  <a class="text-decoration-none text-dark" href="{{ url('/do-choi') }}">Sản phẩm đồ chơi</a> >
  <a class="text-decoration-none text-dark" href="#"> 3-6 tháng</a> >
  <a class="text-decoration-none text-dark" href="">Tên sản phẩm</a>
</p>
<!-- Info -->
<div class="mt-3">
  <!-- TItle -->
  <div class="responsive-width65-80-95 mx-auto">
    <div class=" d-flex justify-content-between">
      <h3 class="font-VLBoosterNextFYBlack title--maxwidth color-red-main size-2rem mb-0">{{ $product->name }}</h3>
      <div>
        @php $rates = $product->rates ? $product->rates : 5 @endphp
        @for($i = 0; $i < $rates; $i++) <i class="fas fa-heart heart--product-title"></i>
          @endfor
          @if(count($product->activeReviews))
          <i>{{ count($product->activeReviews) }}</i>
          @endif
      </div>
    </div>
    <p></p>
    <p>{{ $product->sku }}</p>

  </div>
  <!-- Carousel -->
  <div class="d-flex justify-content-center bg-transparent">

    <!-- Slide sản phẩm dọc  -->
    <div class="product-images py-3 slide-sanpham-doc d-none d-md-flex">

      @php
      $image = $product->image ? $product->image : 'uploads/products/place_holder.jpg';
      $galleries = json_decode($product->galleries);
      $images = [$image];
      if(is_array($galleries) && count($galleries)) {
      $images = array_merge($images, $galleries);
      }
      @endphp
      @foreach($images as $galleryImage)
      <div class=" text-center ">
        <img src="{{ url($galleryImage) }}" class="image--slider-size mx-auto" alt="" srcset="">
      </div>
      @endforeach


      @if($product->video)
        <div class="text-center " style="position: relative">
          <video class="play_button text-center mx-auto w-50" style="z-index: 5;">
            <source src="{{ url($product->video)  }}" type="video/mp4">
          </video>
          <div class="rounded-circle d-flex justify-content-center align-items-center play-icon-col-slide">
            <i class="fa fa-play" style="transform: translateX(1px)" aria-hidden="true"></i>
          </div>
        </div>s
      @endif

    </div>

    <!-- Slide sản phẩm show   -->
    <div class="product-images-big py-3 slide-sanpham-chinh">
      @php
      $image = $product->image ? $product->image : 'uploads/products/place_holder.jpg';
      $galleries = json_decode($product->galleries);
      $images = [$image];
      if(is_array($galleries) && count($galleries)) {
      $images = array_merge($images, $galleries);
      }
      @endphp
      @foreach($images as $galleryImage)
      <div class=" text-center ">
        <img src="{{ url($galleryImage) }}" class="image--main-slider-size mx-auto" alt="" srcset="">
      </div>
      @endforeach

      @if($product->video)
        <div class="text-center position-relative">
          <video id="play_button_bigScreen" class=" play_button mx-auto image--main-slider-size" controls
                 style="z-index: 9;">
            <source src="{{ url($product->video)  }}" type="video/mp4">
          </video>
          <div id="video_play_icon">
            <div class="rounded-circle d-flex justify-content-center align-items-center play-icon-main-slide">
              <i class="fa fa-play" style="transform: translateX(1px)" aria-hidden="true"></i>
            </div>
          </div>

        </div>
      @endif
    </div>
  </div>

</div>

@if($product->description)
<!-- Giới thiệu sản phản -->
<div class="responsive-width65-80-95 mx-auto mb-5">
  <p class=" underline-color font-VLBoosterNextFYBlack color-blue-main text-uppercase size-21px">Giới thiệu sản
    phẩm</p>
  <div class="row">
    @php
    $description = $product->description;
    $descArrays = json_decode($description);
    if(is_array($descArrays) &&count($descArrays)) {

    }
    @endphp
    <div class="col-12 col-lg-6">
      @foreach($descArrays as $desc)
        @if(isset($desc->col) && $desc->col == 1)
          <p>{{ $desc->desc }}</p>
        @endif
      @endforeach
    </div>
    <div class="col-12 col-lg-6">
      @foreach($descArrays as $desc)
        @if(isset($desc->col) && $desc->col == 2)
          <p>{{ $desc->desc }}</p>
        @endif
      @endforeach
    </div>
  </div>
</div>
@endif
<!-- Thương hiệu & Giải thưởng -->

@if($product->brand)
<div class="responsive-width65-80-95 mx-auto mb-5">
  <p class=" underline-color font-VLBoosterNextFYBlack color-blue-main text-uppercase size-21px">
    THƯƠNG HIỆU & GIẢI THƯỞNG
  </p>

  <div class="row">
    <div class="col-12 col-lg-6">
      <p class="text-uppercase">{{ $product->brand->name }}</p>
      <p>{{ $product->brand->description }}</p>
    </div>
    @if($product->brand->reward_galleries)

    <div class="col-12 col-lg-6">
      <p class="text-uppercase">
        Giải thưởng
      </p>
      @php
      $rewardGalleries = json_decode($product->brand->reward_galleries);
      @endphp
      <div>
        @foreach($rewardGalleries as $image)
        <img src="{{ url($image) }}" style="display: inline-block; width: 20%; margin-right:1rem" alt="" srcset="">
        @endforeach
      </div>
    </div>
    @endif
  </div>
</div>
@endif

<!-- Phản hồi trải nghiệm -->

<div class="responsive-width65-80-95 mx-auto mb-5">
  <div class="d-flex justify-content-between underline-color mb-4">
    <div>
      <p class="mb-0 size-21px font-VLBoosterNextFYBlack color-blue-main text-uppercase">
        Phảm hồi trải nhiệm
      </p>
      <span>
        Theo thứ tự thời gian sớm nhất
      </span>
    </div>
    <div>
      <!-- <button class="text-uppercase viet-phan-hoi-button"> -->
      <div class="btn viet-phan-hoi-button ">
        <p class="text-uppercase mb-0">
          Viết phản hồi
        </p>
      </div>
      <!-- </button> -->
    </div>
  </div>

  @if($product->activeReviews && count($product->activeReviews))
  @php
  $reviews = $product->activeReviews()->paginate(3);
  @endphp
  <div>
    @foreach($reviews as $review)
    <!-- 1st comemnt -->
    <div class="mb-4">
      <div class="d-flex justify-content-between">
        <h5 class="font-weight-bold">
          {{ $review->title }}
        </h5>
        <div>
          @for($i = 0; $i < $review->rates; $i++)
            <i class="fas fa-heart"></i>
            @endfor
        </div>
      </div>

      <p>
        {{ $review->comment }}
      </p>
      <small class="d-block  text-right">
        Gửi bởi {{ $review->user_name }}, {{ date('H', strtotime($review->created_at)) }} giờ ngày
        {{ date('d/m/Y', strtotime($review->created_at)) }}
      </small>
    </div>
    @endforeach
    <!-- Pagination -->
    {{ $reviews->links('web.layout.component.pager', ['collection' => $reviews]) }}
    @endif
  </div>
</div>

@if($sameBrandProducts = $product->sameBrandProducts())
<!-- Snả phẩm cùng thương hiệu -->
<div class="" style="border-bottom: 1px solid rgba(44, 70, 102, 1) ;">
  <p class="mx-auto text-uppercase font-VLBoosterNextFYBlack color-blue-main size-21px" style="width: 95%;">
    Sản phẩm cùng thương hiệu
  </p>
</div>
<div class="my-5 row row-products ">
  @foreach($sameBrandProducts as $sameProduct)
  <div class="card col-sm-6 col-lg-3">
    <a href="{{ $sameProduct->getProductLink() }}">
      <img src="{{ $sameProduct->getImageLink() }}" class="card-img-top image--sanpham-slide" alt="...">
      <div class="card-body">
        <div class="d-flex justify-content-between">
          <p class="card-title sanphamlienquan--sizing text-dark">{{ $sameProduct->name }}</p>
          <div class="sanphamlienquan-sizing ">
            @php $rates = $sameProduct->rates ? $sameProduct->rates : 5 @endphp
            @for($i = 0; $i < $rates; $i++) <i class="fas fa-heart heart--product-style"></i>
              @endfor
          </div>
        </div>
        <p class="card-text text-dark">{{ $sameProduct->brand ? $sameProduct->brand->name  : '' }}</p>
      </div>
    </a>
  </div>
  @endforeach
</div>
@endif

<!-- Sản phẩm liên quan -->
@if($product->relatedProducts && count($product->relatedProducts))
<div class="" style="border-bottom: 1px solid rgba(44, 70, 102, 1) ;">
  <p class="mx-auto text-uppercase font-VLBoosterNextFYBlack color-blue-main size-21px" style="width: 95%;">
    Sản phẩm liên quan </p>
</div>
<div class="mb-5 row row-products ">
  @foreach($product->relatedProducts as $relatedProduct)
  <div class="card col-sm-6 col-lg-3">
    <a href="{{ $relatedProduct->getProductLink() }}">
      <img src="{{ $relatedProduct->getImageLink() }}" class="card-img-top image--sanpham-slide"
        style="position: relative;" alt="...">
      <div class="moi-circle-position">
        <div class="moi-circle">Mới</div>
      </div>
      <div class="card-body">
        <div class="d-flex justify-content-between">
          <h5 class="card-title sanphamlienquan--sizing text-dark">{{ $relatedProduct->name }}</h5>
          <div>
            @php $rates = $relatedProduct->rates ? $relatedProduct->rates : 5 @endphp
            @for($i = 0; $i < $rates; $i++) <i class="fas fa-heart heart--product-style"></i>
              @endfor
          </div>
        </div>
        <p class="card-text text-dark">{{ $relatedProduct->brand ? $relatedProduct->brand->name  : '' }}</p>
      </div>
    </a>
  </div>
  @endforeach
</div>
@endif

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
  integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
  integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
  integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<!-- Slick -->
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script>
const mediaPlayer = document.querySelector('#play_button_bigScreen');
mediaPlayer.addEventListener('click', () => {
  if (mediaPlayer.paused) {
    $('#video_play_icon').hide();
  } else {
    $('#video_play_icon').show();
  }
})

const mediaButton = document.querySelector('#video_play_icon');
mediaButton.addEventListener('click', () => {
  if (mediaPlayer.paused) {
    mediaPlayer.play()
    $('#video_play_icon').hide();
  } else {
    mediaPlayer.pause()
    $('#video_play_icon').show();
  }
})
</script>
<script>
$('.product-images').slick({
  dots: false,
  slidesToShow: 5,
  slidesToScroll: 1,
  infinite: true,
  vertical: true,
  verticalSwiping: true,
  asNavFor: '.product-images-big',
  focusOnSelect: true,
  lazyLoad: 'ondemand',
});
$('.product-images-big').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  lazyLoad: 'ondemand',
  autoPlay: true,
  autoplaySpeed: 10000,

});
</script>
@endsection
