@extends('web.layout.1-column')

@section('title', 'Góc chia sẻ')

@section('customs')
    <link rel="stylesheet" href="{{ url('css/blog.css') }}"/>
@endsection

@section('content')
    <br/>
    <br/>
    <br/>
    <main class=" d-block d-md-flex position-relative" style="width: 95%; margin: 0 auto 2rem;">
        <!-- Side bar -->
        <div class="sidebar--reponsive ">
            <div class="sticky-sidebar">
                <!-- Học & Chơi -->
                @if($categories && count($categories))
                    <div class="sidebar--item-responsive">
                        @foreach($categories as $category)
                            @php
                                $class = request()->get('category_id') == $category->id ? 'active' : '';
                            @endphp
                            <p class="text-uppercase text--hover-bold"
                               style="border-bottom:2px solid {{ $category->color }}; margin-right:2rem">
                                <a class="{{ $class }}" style="color: black;"
                                   href="{{ url('goc-kheo-tay/chu-de/' . $theme->slug .'?category_id=' . $category->id) }}">{{ $category->title }}</a>
                            </p>
                        @endforeach
                    </div>
            @endif

            <!-- Thời kỳ phát triển -->
                <div class="d-flex d-md-block">
                    <!-- Bài đăng theo tháng -->
                    <div class="sidebar--item-responsive ">
                        <p class="text-uppercase">bài đăng theo tháng</p>
                        <div class="calender-custom">
                            @for($i = 1; $i <= 12; $i++) @php $class=request()->get('month') == $i ? 'active' : '';
                            @endphp
                            @if($i % 4 == 1)
                                <div class="d-flex justify-content-between align-items-center"
                                     style="margin-bottom: 0.25rem ;">
                                    @endif
                                    <div class="calender-table {{ $class }}">
                                        <a class="{{ $class }} linkTrongCalender"
                                           href="{{ url('goc-kheo-tay/chu-de/' . $theme->slug .'?month='. $i) }}">{{ $i }}</a>
                                        <!-- Add lại calender-table -->
                                    </div>
                                    @if($i % 4 == 0)
                                </div>
                            @endif
                            @endfor
                        </div>
                    </div>
                </div>

                <div class="sidebar-blog-detail-item-responsive">
                    <p class="text-uppercase">Theo độ tuổi</p>
                    <div class="calender-custom">
                        <div class="d-flex justify-content-between align-items-center" style="margin-bottom: 0.25rem ;">
                            <div class="calender-table">
                                <a class=" linkTrongCalender">1</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Các loại blog -->
        <div class="" style="flex: 1;">
    <span class="span--dieuhuong-menu">
      <a class="text--hover-bold" style="color: black" href="{{ url('trang-chu') }}">Trang chủ</a> >
      <a class="text--hover-bold" style="color: black" href="{{ url('goc-kheo-tay') }}">Góc khéo tay</a> >
        <a class="text--hover-bold" style="color: black" >{{ $theme->title }}</a>
    </span>
            @if(count($rows))
                @foreach($rows as $row)
                    <div class="row my-3">
                        @foreach($row as $item)
                            <div class="col-12 col-lg-{{ $item->type == 2 ? 8 : 4 }} ">
                                <a href="{{ $item->getDiyLink() }}">
                                    <img src="{{ url($item['image']) }}" class="mw-100 w-100 blog-image" alt=""
                                         srcset=""/>
                                </a>
                                <div class="blog--image-introduce">
                                    <p class="blog--image-title mb-0 "
                                       style="background-color: {{ $item->category ? $item->category->color : '' }};">
                                        {{ $item->category ? $item->category->title : ''}}
                                    </p>
                                    <p class="flex-grow-1 size-18px">
                                        <a href="{{ $item->getDiyLink() }}" class="text-dark title--blog-bold">
                                            {{ $item->name }}
                                        </a>
                                    </p>
                                    <small>{{ date('M d, Y', strtotime($item->created_at)) }}</small>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach

                {{ $blogs->links('web.layout.component.pager', ['collection' => $blogs]) }}
            @else
                <div class="row my-3" style="padding-left: 18px; color: #DDD">
                    Chưa có bài viết
                </div>
            @endif
        </div>
        <!-- Side bar -->

        <!-- BLOG -->
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{ url('js/bootstrap/bootstrap.min.js') }}"
                integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
                crossorigin="anonymous">
        </script>
    </main>
@endsection
