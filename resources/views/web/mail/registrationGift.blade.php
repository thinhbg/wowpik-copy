@php
    $data = [
        'Loại' => $registration->price_option,
        'Họ tên của bạn' => $registration->customer_name,
        'Điện thoai' => $registration->customer_phone,
        'Họ tên của bé' => $registration->child_name,
        'Tên ở nhà của bé' => $registration->child_home_name,
        'Họ tên người nhận quà' => $registration->receiver_name,
        'Địa chỉ người nhận quà' => $registration->receiver_adddress,
        'Thành phố (người nhận)' => $registration->receiver_city,
        'Quận (người nhận)' => $registration->receiver_state,
        'Phường (người nhận)' => $registration->receiver_phuong,


        'Email' => $registration->customer_email,
        'Ngày sinh' => $registration->child_dob_year . '-' . $registration->child_dob_month . '-' . $registration->child_dob_day  ,
        'Diện thoại(người nhận)' => $registration->receiver_phone,
        'Lời nhắn' => $registration->receiver_note,
    ];
@endphp

<h3>Bạn có đăng ký mới, vui lòng truy cập {{ url('admin/registrations_gift/' . $registration->id . '/edit') }}</h3>
<ul>
    <li>ID: {{ $registration->id }}</li>
    @foreach($data as $label => $value)
        <li>{{ $label }}: {{ $value }}</li>
    @endforeach
</ul>