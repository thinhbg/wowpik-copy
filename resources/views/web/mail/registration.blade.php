@php
    $data = [
        'Loại' => $registration->price_option,
        'Họ tên của bé' => $registration->child_name,
        'Tên ở nhà của bé' => $registration->child_home_name,
        'Họ tên của bạn' => $registration->customer_name,
        'Email' => $registration->customer_email,
        'Điện thoai' => $registration->customer_phone,
        'Ngày sinh' => $registration->child_dob_year . '-' . $registration->child_dob_month . '-' . $registration->child_dob_day  ,
        'Địa chỉ nhận quà' => $registration->customer_address,
        'Thành phố' => $registration->customer_city,
        'Quận' => $registration->customer_state,
        'Phường' => $registration->customer_phuong
    ];
@endphp

<h3>Bạn có đăng ký mới, vui lòng truy cập {{ url('admin/registrations/' . $registration->id . '/edit') }}</h3>
<ul>
    <li>ID: {{ $registration->id }}</li>
    @foreach($data as $label => $value)
        <li>{{ $label }}: {{ $value }}</li>
    @endforeach
</ul>