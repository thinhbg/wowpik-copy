@extends('web.layout.1-column')

@section('title', 'Đồ chơi')

@section('customs')
<link rel="stylesheet" href="css/product.css" />

@endsection

@section('content')
<!-- Text Giới thiệu free -->
<div class="green-background py-1 mb-0 mb-lg-5 text-left">
  <h3 class="freeship-font-responsive text-white text-center mb-0 ">
    {{ Setting::get('product_promotion_title') }}
  </h3>
</div>
<!--  BOdy  -->
<div class="mx-auto" style="width: 90%;">
  <div class="row">
    <!-- Row filter -->
    <div id="filters-block" class="col-12 col-lg-3 border-red">
      <div id="accordion" style="position:sticky; top:70px">
        <div class="filters-control green-background" id="filters-control-header">
          <i class="fas fa-minus">Bộ lọc</i>
        </div>
        @foreach($filters as $key => $filter)
          <div class="filter--products" style="">
            <div class="" id="headingOne">
              <h5
                class="btn text-uppercase text-left w-100 d-flex align-items-center justify-content-between my-0 border--title-filter"
                data-toggle="collapse" data-target="#collapseOne-{{$key}}" aria-expanded="true"
                aria-controls="collapseOne-{{$key}}">
                {{ $filter['label'] }}
                <div class="d-inline-block ">
                  <i class="fas fa-plus"></i>
                </div>
              </h5>
            </div>

            <div id="collapseOne-{{ $key }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion"
              style="">
              <div class="card-body">
                @foreach($filter['options'] as $key => $option)
                @php
                $class = request()->get($filter['key']) == $key ? 'active' : '';
                @endphp
                <a class="filter-link {{ $class }}" href="{{ url("do-choi?{$filter['key']}=$key") }}">{{ $option }} </a>
                <br />
                @endforeach
              </div>
            </div>
          </div>
        @endforeach
      </div>
      <!-- end col-3 -->
    </div>

    <style>
    .filter-link.active {
      font-weight: bold;
    }
    </style>
    <!-- Các row Sản phẩm -->
    <div class="filters-control col-12 card-header" id="filters-control-button">
      <i class="fas fa-plus">Bộ lọc</i>
    </div>
    <div class="col-sm-12 col-lg-9 border-red">
      <div class="row">
        <div class="col-12">
          <p>Trang chủ > Sản phẩm đồ chơi > Chơi & học</p>
          <h3 class="text-uppercase font-VLBoosterNextFYBlack color-blue-main">
            đồ chơi chính hãng
          </h3>
        </div>
      </div>
      @php
      $count = 0;
      $numRow = 4;
      @endphp
      @foreach($products as $product)
      @if($count % $numRow == 0)
      <div class="row">
        @endif
        <div class="col-sm-6 col-lg-3 border-red">
        <!-- //FIXME: phần này khách kêu bị hiển thị mất 2 item cuối  -->
          <div class="product-box ">
            <a href="{{ $product->getProductLink() }}">
              <img src="{{ $product->getImageLink() }}" class="images-product" alt="" srcset="" />
            </a>
            <a href="{{ $product->getProductLink() }}">
              <div class="d-flex justify-content-between">
                <p class="text-dark font-weight-bold title--products-size">{{ $product->name }}</p>
                <div>
                  @php $rates = $product->rates ? $product->rates : 5 @endphp
                  @for($i = 0; $i < $rates; $i++) <i class="fas fa-heart heart--product-style"></i>
                    @endfor
                </div>
              </div>
              @if($product->brand)
              <p class="text-dark text--introduce-product">{{ $product->brand->name }}</p>
              @endif
            </a>
          </div>
        </div>
        @if($count % $numRow == $numRow - 1)
      </div>
      @endif
      @php $count++ @endphp
      @endforeach

    </div>
    {{ $products->links('web.layout.component.pager', ['collection' => $products]) }}
  </div>
</div>
<!-- Carousel  -->

@if($slider && count($slider))
<div class="green-background" style="margin-bottom: 3rem;">
  <p class="font-VLBoosterNextFYBlack color-blue-main carousel-product-title">
    Đồ chơi chính hãng với những thương hiệu quốc tế
  </p>
  <div class="d-flex flex-row">
    <button class="previousButton-toys button--next-previous-product">
      <i class="fas fa-caret-left fa-3x" style="color: white;"></i>
    </button>

    <div class="multiple-items-toys carousel py-5 flex-grow-1" id="carouselExampleControls" data-ride="carousel"
      style="width: 1px;">
      @foreach($slider as $slide)
      <div>
        <a href="{{ $slide->brand ? $slide->brand->getBrandLink() : '#' }}">
          <img src="{{ url($slide->image) }}" alt="" srcset="" class="carousel-products mx-auto" />
        </a>
      </div>
      @endforeach
    </div>
    <button class="nextButton-toys button--next-previous-product">
      <i class="fas fa-caret-right fa-3x" style="color: white;"></i>
    </button>
  </div>
</div>
@endif

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
{{--    <script--}}
{{--            src="https://code.jquery.com/jquery-3.5.1.slim.min.js"--}}
{{--            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"--}}
{{--            crossorigin="anonymous"--}}
{{--    ></script>--}}
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
  integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
  integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<!-- Slick -->
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script type="text/javascript">
$('.filters-control').on('click', function() {
  $('#filters-block').toggle();
});

$('#filters-block').find('.collapse').on('show.bs.collapse', function() {
  $(this).prev().find('i.fas.fa-plus').removeClass('fa-plus').addClass('fa-minus');
});

$('#filters-block').find('.collapse').on('hidden.bs.collapse', function() {
  $(this).prev().find('i.fas.fa-minus').removeClass('fa-minus').addClass('fa-plus');
});

$('.multiple-items-toys').slick({
  infinite: true,
  slidesToShow: 6,
  slidesToScroll: 1,
  // arrows: true,
  prevArrow: $('.previousButton-toys'),
  nextArrow: $('.nextButton-toys'),
  autoplay: true,
  autoplaySpeed: 3000,
  responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 2,
        infinite: true,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
})
</script>
@endsection
