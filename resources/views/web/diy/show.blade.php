@extends('web.layout.1-column')

@section('title', $diy->name)

@section('customs')
  <link rel="stylesheet" href="{{ url('css/blog.css') }}" />
  <link rel="stylesheet" href="{{ url('css/blog-detail.css') }}" />
@endsection

@section('content')
  <br />
  <br />
  <br />
  <main class="d-block d-md-flex position-relative" style="width: 95%; margin: 0 auto 2rem;">
    <!-- Side bar -->
    <div class="sidebar--blog-detail-reponsive">
      <div class="sticky-sidebar">
        <!-- Học & Chơi -->
        @if($categories && count($categories))
          <div class="sidebar-blog-detail-item-responsive">
            @foreach($categories as $category)
              @php
                $class = request()->get('category_id') == $category->id ? 'active' : '';
              @endphp
              <p class="text-uppercase text--hover-bold"
                 style="border-bottom:2px solid {{ $category->color }}; margin-right:1rem">
                <a class="{{ $class }}" style="color: black;"
                   href="{{ url('goc-kheo-tay/chu-de/' . $diy->theme()->slug. '?category_id=' . $category->id) }}">{{ $category->title }}</a>
              </p>
            @endforeach
          </div>
        @endif

        <div class="d-flex d-md-block">
          <!-- Thời kỳ phát triển -->
          <!-- Bài đăng theo tháng -->
          <div class="sidebar-blog-detail-item-responsive">
            <p class="text-uppercase">bài đăng theo tháng</p>
            <div class="calender-custom">
              @for($i = 1; $i <= 12; $i++) @php $class=request()->get('month') == $i ? 'active' : '';
              @endphp
              @if($i % 4 == 1)
                <div class="d-flex justify-content-between align-items-center" style="margin-bottom: 0.25rem ;">
                  @endif
                  <div class="calender-table {{ $class }}">
                    <a class="{{ $class }} linkTrongCalender" href="{{ url('goc-kheo-tay/chu-de/' . $diy->theme()->slug. '?month='. $i) }}">{{ $i }}</a>
                    <!-- Add lại calender-table -->
                  </div>
                  @if($i % 4 == 0)
                </div>
              @endif
              @endfor
            </div>
          </div>

        <div class="social-button-sharing">
          <div class=" mb-3">
            <a href="{{ Setting::get('header_facebook_title') }}"
               class="facebook-hover decoration-background-border-radius d-flex justify-content-center align-items-center">
              <i class="fab fa-facebook-f" style="width: 0.5rem !important;" aria-hidden="true"></i>
            </a>
          </div>
          <div class="mb-3">
            <a href="{{ Setting::get('header_instagram_title') }}"
               class="instagram-hover decoration-background-border-radius d-flex justify-content-center align-items-center">
              <i class="fab fa-instagram" aria-hidden="true">
              </i>
            </a>
          </div>
          <div class="mb-3">
            <a href="mailto:{{ Setting::get('header_email_title') }}"
               class="youtube-hover decoration-background-border-radius d-flex justify-content-center align-items-center">
              <i class="far fa-envelope" aria-hidden="true"></i>
            </a>
          </div>
          <div class="mb-3">
            <a href="{{ Setting::get('header_youtube_title') }}"
               class="facebook-hover decoration-background-border-radius d-flex justify-content-center align-items-center">
              <i class="fab fa-linkedin-in" aria-hidden="true"></i>
            </a>
          </div>
        </div>
      </div>
    </div>


    <div style="flex:1">
    <span class="span--dieuhuong-menu">
      <a class="text--hover-bold" style="color: black" href="{{ url('/') }}">Trang chủ</a> &gt;
      <a class="text--hover-bold" style="color: black" href="{{ url('goc-kheo-tay') }}">Góc khéo tay</a> &gt;
      <a class="text--hover-bold" style="color: black" href="{{ url('goc-kheo-tay/chu-de/' . $diy->theme()->slug ) }}">{{ $diy->theme()->title }}</a> &gt;
      <a class="text--hover-bold" style="color: black">{{ $diy->title }}</a>
    </span>
      <div>
        @if($diy->category)
          <p class="title" style="background-color: {{ $diy->category->color }}">{{ $diy->category->title }}</p>
        @endif
        <h3 class="">
          {{ $diy->name }}
        </h3>
        <p class="">
          {{ date('l', strtotime($diy->created_at)) }} ngày {{ date('m/d/Y', strtotime($diy->created_at)) }}
        </p>
        @if($diy->video)
          <video width="100%" height="100%" controls="" autoplay="" src="{{ url($diy->video) }}"></video>
        @endif

      </div>
      <div id="blog-main-content">
        {!! $diy->content !!}

        <br/>



      <div class="downloadArea">

      <a href="{{ $diy->getDownloadLinK() }}">

      <div class="btn background-red download__button text-white text-uppercase mr-2">
          download file thiết kế
        </div>
        </a>


        <a href="{{ $diy->video_guide_link }}" target="_blank">
        <div class="btn background-red download__button text-white text-uppercase">
          xem video hướng dẫn
        </div>
        </a>
      </div>
    </div>
      <br>
        {!! $diy->writer !!}
      </div>
    </div>
    <!-- Side bar -->

    <!-- BLOG -->
  </main>

  @if($diy->relatedDiy && count($diy->relatedDiy))
    <div class="mx-auto" style="width: 90%;margin-bottom: 5rem;">
      <p style="font-size: 2rem; " class="">Các bài liên quan</p>
      <div class="d-flex justify-content-around">
        @foreach($diy->relatedDiy as $relatedBlog)
          <div class="mx-3 ">
            <a href="#" class="text-dark">
              <img src="{{ url($relatedBlog->image) }}" class="mw-100 w-100 blog-image" alt="" srcset="">
            </a>
            <a href="{{ $relatedBlog->getDiyLink() }}" class="text-dark">
              <div class="blog--image-introduce">
                {{--                                <p class="blog--image-title" style="background-color: {{ $relatedBlog->category ? $relatedBlog->category->color : '' }};">--}}
                {{--                                    {{ $relatedBlog->category ? $relatedBlog->category->title : ''}}--}}
                {{--                                </p>--}}
                <p><a href="{{ $relatedBlog->getDiyLink() }}">{{ $relatedBlog->title }}</a></p>
                <small>{{ date('M d, Y', strtotime($relatedBlog->created_at)) }}</small>
              </div>
            </a>
          </div>
        @endforeach
      </div>
    </div>
  @endif
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="{{ url('js/bootstrap/bootstrap.min.js') }}"
          integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
@endsection
