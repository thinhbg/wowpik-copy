@extends('web.layout.1-column')

@section('title', 'Đăng nhập')
@section('customs')
<link rel="stylesheet" href="{{ url('css/authentication.css') }}" />
@endsection
@section('content')
<!-- 4 Button đăng ký -->
<div class="background--dangKyDangNhap">
  <div class="dangKyArea mx-auto">
    <div class="dangNhap--buttons">

      <div class="row justify-content-center">
        <div class="col-6 text-center mx-auto">
          <a href="{{ url('dang-ky') }}"
            class="btn  dangKyButton mx-auto d-flex align-items-center justify-content-center ">
            <span class="text-uppercase font-24px-responsive">
              Hội viên mới
            </span>
          </a>
        </div>
        <div class="col-6 text-center mx-auto">
          <a class="btn  dangKyButton mx-auto d-flex align-items-center justify-content-center ">
            <span class="text-uppercase font-24px-responsive">
              đăng nhập
            </span>
          </a>
        </div>
      </div>

      <div class="dangNhapTextArea">
        <form action="{{ route('login') }}" method="POST">
          @csrf
          <div class="form-group-custom">
            <label class="" style="font-size: 1.5rem;">Email*</label>
            <input name="email" type="email" class="form-control-custom-dangKy" aria-describedby="emailHelp"
              placeholder="Nhập email" required>
          </div>
          <div class="form-group-custom">
            <label class="" style="font-size: 1.5rem;">Mật khẩu*</label>
            <input name="password" type="password" class="form-control-custom-dangKy" placeholder="Nhập mật khẩu"
              required>
          </div>

          <div class="d-flex justify-content-end">
            <div class="div-rong-custom"></div>
            <a href="#QuenMatKhau" class="quenMatKhau">Quên mật khẩu</a>
            <button type="submit" class="btn submit-btn text-uppercase">Đăng Nhập</button>
          </div>

        </form>
      </div>
    </div>

  </div>
  @endsection
