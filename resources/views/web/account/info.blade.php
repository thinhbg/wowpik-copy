@extends('web.layout.1-column')

@section('title', 'Thông tin cá nhân')

@section('customs')
    <link rel="stylesheet" href="{{ url('css/profile.css') }}" />
@endsection

@section('content')
    <main class="row" style="width: 95%; margin: 2rem auto ;">
        <!-- Side bar -->
        <div class="col-12 col-lg-2">
            <div class="mb-5">
                <a href="#" class="mb-3  d-block text-uppercase profile--sidebar ">
                    TÀI KHOẢN CỦA TÔI
                </a>
{{--                <a href="#" class="mb-3  d-block text-uppercase profile--sidebar">--}}
{{--                    LỊCH SỬ ĐƠN HÀNG--}}
{{--                </a>--}}
{{--                <a href="#" class="mb-3  d-block text-uppercase profile--sidebar">--}}
{{--                    HOẠT ĐỘNG--}}
{{--                </a>--}}
{{--                <a href="#" class="mb-3  d-block text-uppercase profile--sidebar">--}}
{{--                    CÀI ĐẶT--}}
{{--                </a>--}}
            </div>
        </div>
        <div class="col-12 col-lg-10">

            <p class="text-uppercase font-weight-bold">
                Thông tin cá nhân
            </p>

            <form action="{{ url('thongtinPost') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-5">
                        <div class="form-group">
                            <label class="text-uppercase font-14px">Họ và tên bạn</label>
                            <input type="text" class="form-control" name="name" value="{{ $user->name }}">
                        </div>
                    </div>
                    <div class="col-1"></div>
                    <div class="col-5">
                    </div>
                </div>
                <div class="row">
                    <div class="col-5">
                        <div class="form-group">
                            <label class="text-uppercase font-14px">Email*</label>
                            <input type="email" class="form-control" required name="email" value="{{ $user->email }}">
                        </div>
                    </div>
                    <div class="col-1"></div>
                    <div class="col-5">
                        <div class="form-group">equired
                            <label class="text-uppercase font-14px">Điện thoại*</label>
                            <input type="text" class="form-control" required name="phone" value="{{ $user->phone }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-5">
                        <div class="form-group">
                            <label class="text-uppercase font-14px">Họ và tên bé</label>
                            <input type="text" class="form-control" name="baby_name" value="{{ $user->baby_name }}">
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <label class="text-uppercase font-14px">Năm sinh</label>
                            <select required class="select-input-custom " name="baby_year">
                                @for($i = date('Y'); $i >= date('Y') - 10; $i--)
                                    <option value="{{ $i }}" {{ $user->baby_year == $i ? 'selected' : '' }}>{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="col-5 nick-name-block">

                            @php
                                $size = 0;
                            @endphp
                            @if(!$user->baby_nicknames)
                                <div class="form-group">
                                    <label class="text-uppercase font-14px">Nick name</label>
                                    <input type="text" class="form-control" name="nick_names[0]" />
                                </div>
                            @else
                                @php $nickNames = json_decode($user->baby_nicknames); @endphp
                                @if(is_array($nickNames) && !empty($nickNames))
                                    @foreach($nickNames as $nick)
                                        <div class="form-group">
                                            <label class="text-uppercase font-14px">Nick name</label>
                                            <input type="text" class="form-control" name="nick_names[{{ $size }}]" value="{{ $nick }}"/>
                                        </div>
                                        @php $size++ @endphp
                                    @endforeach
                                @endif
                            @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-5"></div>
                    <div class="col-1"></div>
                    <div class="col-5 ">
                        <span id="addMoreNickName" class="btn add-More">Thêm nick name</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-5">
                        <div class="form-group">
                            <label class="text-uppercase font-14px">Sở thích của bé</label>
                            <textarea class="form-control" name="baby_hobbies">{{ $user->baby_hobbies }}</textarea>
                        </div>
                    </div>
                    <div class="col-1"></div>
                    <div class="col-5">
                        <div class="form-group">
                            <label class="text-uppercase font-14px">Lưu ý khác</label>
                            <textarea class="form-control" name="notes">{{ $user->notes }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    <button type="submit" class="btn submit-button text-uppercase">Lưu thay đổi</button>
                    <div class="col-1">
                    </div>
                </div>
            </form>


{{--            <p class="text-uppercase font-weight-bold">--}}
{{--                Sổ địa chỉ--}}
{{--            </p>--}}
{{--            <form action="{{ url('diachiPost') }}" method="POST">--}}
{{--                <div id='address-block'>--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-5">--}}
{{--                            <div class="form-group">--}}
{{--                                <label class="text-uppercase font-14px">địa chỉ*</label>--}}
{{--                                <input type="text" class="form-control" required name="addresses[0]['address']" />--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-1"></div>--}}
{{--                        <div class="col-5">--}}
{{--                            <div class="form-group">--}}
{{--                                <label class="text-uppercase font-14px">Phường/Xã</label>--}}
{{--                                <select required class="select-input-custom " name="addresses[0]['address_phuong']" />--}}
{{--                                    <option selected></option>--}}
{{--                                    <option value="1">Phường hàng đào</option>--}}
{{--                                    <option value="2">Phường hàng đào</option>--}}
{{--                                    <option value="3">Phường hàng đào</option>--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="row">--}}
{{--                        <div class="col-5">--}}
{{--                            <div class="form-group">--}}
{{--                                <label class="text-uppercase font-14px">Quận/Huyện</label>--}}
{{--                                <select required class="select-input-custom " name="addresses[0]['address_state']" />--}}
{{--                                    <option selected></option>--}}
{{--                                    <option value="1">Phường hàng đào</option>--}}
{{--                                    <option value="2">Phường hàng đào</option>--}}
{{--                                    <option value="3">Phường hàng đào</option>--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-1">--}}

{{--                        </div>--}}
{{--                        <div class="col-5">--}}
{{--                            <div class="form-group">--}}
{{--                                <label class="text-uppercase font-14px">Quốc gia</label>--}}
{{--                                <input type="text" class="form-control" required name="addresses[0]['address_country']" />--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="row">--}}
{{--                    <div class="col-5"></div>--}}
{{--                    <div class="col-1"></div>--}}
{{--                    <div class="col-5 ">--}}
{{--                        <span id="addMoreAddress" class="btn add-More">Thêm Địa chỉ</span>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="d-flex justify-content-end">--}}
{{--                    <button type="submit" class="btn submit-button text-uppercase">Lưu thay đổi</button>--}}
{{--                    <div class="col-1">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </form>--}}

        </div>

    </main>
    <script>
        let clickCounter = 0;
        let size = {{ $size }}
        $('#addMoreNickName').click(function (e) {
            e.preventDefault();
            size++;
            $(".nick-name-block").append(
                '<div class="form-group">'
                + '<label class="text-uppercase font-14px">Nick name</label>'
                + '<input type="text" class="form-control" name="nick_names[' + size +']">'
                + '</div>'
            );
            clickCounter += 1;
            if (clickCounter >= 2) {
                $('#addMoreNickName').parent().remove()
            }
        })

        let clickAddressCounter = 0;
        $('#addMoreAddress').click(function (e) {
            e.preventDefault();
            $("#address-block").append(
                ' <div class="row"> '
                + '       <div class="col-5"> '
                + '         <div class="form-group"> '
                + '           <label class="text-uppercase font-14px">địa chỉ</label> '
                + '           <input type="text" class="form-control" required> '
                + '         </div> '
                + '       </div> '
                + '       <div class="col-1"></div> '
                + '       <div class="col-5"> '
                + '         <div class="form-group"> '
                + '           <label class="text-uppercase font-14px">Phường/Xã</label> '
                + '           <select required class="select-input-custom "> '
                + '             <option selected></option> '
                + '             <option value="1">Phường hàng đào</option> '
                + '             <option value="2">Phường hàng đào</option> '
                + '             <option value="3">Phường hàng đào</option> '
                + '           </select> '
                + '         </div> '
                + '       </div> '
                + '     </div> '
                + '     <div class="row"> '
                + '       <div class="col-5"> '
                + '         <div class="form-group"> '
                + '           <label class="text-uppercase font-14px">Quận/Huyện</label> '
                + '           <select required class="select-input-custom "> '
                + '             <option selected></option> '
                + '             <option value="1">Phường hàng đào</option> '
                + '             <option value="2">Phường hàng đào</option> '
                + '             <option value="3">Phường hàng đào</option> '
                + '           </select> '
                + '         </div> '
                + '       </div> '
                + '       <div class="col-1"> '
                + '       </div> '
                + '       <div class="col-5"> '
                + '         <div class="form-group"> '
                + '           <label class="text-uppercase font-14px">Quốc gia</label> '
                + '           <input type="text" class="form-control" required> '
                + '         </div> '
                + '       </div> '
                + '     </div> '
            );
            clickAddressCounter += 1;
            if (clickAddressCounter >= 2) {
                $('#addMoreAddress').parent().remove()
            }
        })


    </script>

@endsection