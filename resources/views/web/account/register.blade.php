@extends('web.layout.1-column')

@section('title', 'Đăng nhập')
@section('customs')
<link rel="stylesheet" href="{{ url('css/authentication.css') }}" />
@endsection
@section('content')
<div class="wowpik--background">
  <div class="dangKyArea mx-auto">
    <div class="dangKy-4buttons">
      <div class="row justify-content-center">
        <div class="col-6 text-center mx-auto">
          <a class="btn  dangKyButton mx-auto d-flex align-items-center justify-content-center ">
            <span class="text-uppercase font-24px-responsive">
              Hội viên mới
            </span>
          </a>
        </div>
        <div class="col-6 text-center mx-auto">
          <a class="btn  dangKyButton mx-auto d-flex align-items-center justify-content-center "
            href="{{ url('dang-nhap') }}">
            <span class="text-uppercase font-24px-responsive">
              đăng nhập
            </span>
          </a>
        </div>
      </div>
      {{--                <div class="row justify-content-center">--}}
      {{--                    <div class="col-6 text-center mx-auto">--}}
      {{--                        <a class="btn position-relative dangKyButton mx-auto d-flex align-items-center justify-content-center ">--}}
      {{--                            <div class="gg-fb-btn-icons">--}}
      {{--                                <img src="/assets/imagesSau28-6/google.svg" class="gg-fb-icon-custom-size" alt="">--}}
      {{--                            </div>--}}
      {{--                            <span class="text-uppercase font-24px-responsive">--}}
      {{--                Google--}}
      {{--              </span>--}}
      {{--                        </a>--}}
      {{--                    </div>--}}
      {{--                    <div class="col-6 text-center mx-auto">--}}
      {{--                        <a class="btn position-relative dangKyButton mx-auto d-flex align-items-center justify-content-center ">--}}
      {{--                            <div class="gg-fb-btn-icons">--}}
      {{--                                <img src="/assets/imagesSau28-6/facebook.svg" class="gg-fb-icon-custom-size" alt="">--}}
      {{--                            </div>--}}
      {{--                            <span class="text-uppercase font-24px-responsive">--}}
      {{--                facebook--}}
      {{--              </span>--}}
      {{--                        </a>--}}
      {{--                    </div>--}}
      {{--                </div>--}}
    </div>

    {{--            <div class="dangKyTextArea">--}}
    {{--                <p class="text-center dangKyText">--}}
    {{--                    Đăng ký với mạng xã hội là siêu nhanh. Không có mật khẩu bổ sung để nhớ - không có lỗi. Đừng lo lắng, thông--}}
    {{--                    tin--}}
    {{--                    của bạn hoàn toàn bảo mật--}}
    {{--                </p>--}}
    {{--            </div>--}}

    <div class="form--dangky">

      <form action="{{ route('register') }}" method="POST">
        @csrf
        <div class="form-group-custom">
          <p class="text-uppercase font-24px mb-0">
            Đăng ký qua email
          </p>
        </div>

        <div class="form-group-custom">

          <label class="font-24px">Email*</label>
          <input name="email" type="email" class="form-control-custom-dangKy" aria-describedby="emailHelp"
            placeholder="Nhập email" required />
        </div>
        <div class="form-group-custom">
          <label class="font-24px">Mật khẩu*</label>
          <input name="password" type="password" class="form-control-custom-dangKy" placeholder="Nhập mật khẩu"
            required />
        </div>

        <div class="d-flex justify-content-end">
          <button type="submit" class="btn submit-btn text-uppercase">Đăng ký</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection