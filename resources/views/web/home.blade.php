@extends('web.layout.1-column')

@section('title', 'Trang chủ')

@section('content')
    <!-- Banner --> <!-- TODO: Redo carousel banner -->
    @if($slider && count($slider->sliders))
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators mx-0">
                @php $count = 0 @endphp
                @for($i = 0; $i < count($slider->sliders); $i++)
                    <li data-target="#carousel" data-slide-to="{{ $i }}" class="{{ $i == 0 ? 'active' : '' }}"></li>
                    @php $count++; @endphp
                @endfor
            </ol>
            <div class="carousel-inner">
                @php $count = 0 @endphp
                @foreach($slider->sliders as $slide)
                    <div class="carousel-item {{ $count == 0 ? 'active' : '' }}">
                        <img class="d-block w-100 carousel-images" src="{{ url($slide->image) }}"/>
                        <div class="khampha-banner">
                            @if($slide->text)
                                <p class="text-index-banner text-center font-VLBoosterNextFYBlack mb-0">
                                    {{ $slide->text }}
                                </p>
                            @endif
                            @if($slide->button_display && $slide->button_text)
                                <div class="btn text-uppercase btn--khampha-banner mx-auto">
                                    <a href="{{ $slide->link ? $slide->link : '#' }}" class="text-white">{{ $slide->button_text }}</a>
                                </div>
                            @endif
                        </div>
                    </div>
                    @php $count++; @endphp
                @endforeach
            </div>
        </div>
    @endif

    @if($toys && count($toys))
        <!-- TextLine between 2 carousel -->
        <div class="d-flex justify-content-center" style="background-color: #2c4666;">
            <p class="my-auto" style="color: white; padding: 0.5rem;">
                {{ Setting::get('home_introduce_title') }}
            </p>
        </div>
        <!-- 500+ đồ chơi -->
        <div class="pt-5 text-center color-245x3">
            <p class="font-VLBoosterNextFYBlack text-center color-blue-main text--fontsize-responsive mb-0" >
                {{  Setting::get('home_toys_title') }}
            </p>
            <!-- ĐỒ chơi chính hãng-->
            <div class="d-flex flex-row">
                <button class="previousButton-toys button--next-previous">
                    <img src="/assets/images/left-arrow.png"
                            class="image--logo-style"
                            alt=""
                            srcset=""/>
                </button>

                <div class="banner-slider multiple-items-toys carousel py-3 flex-grow-1"
                        id="carouselExampleControls"
                        data-ride="carousel"
                        style="width: 1px;">
                    @foreach($toys as $toy)
                        <div>
                            <a href="{{ $toy->product ? $toy->product->getProductLink() : '#' }}">
                                <img
                                        data-lazy="{{ url($toy->image) }}"
                                        data-src="{{ url($toy->image) }}"
                                        alt=""
                                        srcset=""
                                        class="carousel-toys-index mx-auto"
                                />
                            </a>
                        </div>
                    @endforeach
                </div>
                <button class="nextButton-toys button--next-previous">
                    <img src="/assets/images/right-arrow.png"
                            class="image--logo-style"
                            alt=""
                            srcset=""/>
                </button>
            </div>
        </div>
    @endif

    @if($brands && count($brands))
        <div class="text-center color-245x3">
            <p class="font-VLBoosterNextFYBlack text-center color-blue-main text--fontsize-responsive mb-0" >
                {{  Setting::get('home_brands_title') }}
            </p>
            <!-- Thuong hiệu quốc tế -->
            <div class="d-flex flex-row">
                <button class="previousButton-brand button--next-previous">
                    <img src="/assets/images/left-arrow.png" class="image--logo-style" alt="" srcset=""/>
                </button>

                <div class="banner-slider multiple-items carousel py-3 flex-grow-1" id="carouselExampleControls" data-ride="carousel" style="width: 1px;">
                    @foreach($brands as $brand)
                        <div style="margin-right: 0;">
                            <a href="{{ $brand->brand ? $brand->brand->getBrandLink() : '#' }}">
                                <img
                                        data-lazy="{{ url($brand->image) }}"
                                        data-src="{{ url($brand->image) }}"
                                     alt=""
                                     srcset=""
                                     class="carousel-brands-index  mx-auto"
                                     />
                            </a>
                        </div>
                    @endforeach
                </div>
                <button class="nextButton-brand button--next-previous">
                    <img src="/assets/images/right-arrow.png"
                            class="image--logo-style"
                            alt=""
                            srcset=""/>
                </button>
            </div>
        </div>
    @endif
    <!-- 30+ thương hiệu quốc tế -->

    @if($countries && count($countries))

        <!-- 10+ quốc gia -->
        <div class="pb-5 color-245x3">
            <p class="font-VLBoosterNextFYBlack text-center color-blue-main text--fontsize-responsive mb-0"
               >
                {{  Setting::get('home_countries_title') }}
            </p>
            <div class="d-flex justify-content-center flex-wrap">
                @foreach($countries as $country)
                    <a href="{{ url('do-choi') . '?country_id=' . $country->id }}">
                        <div class="national-div"
                             style="background: url('{{ url($country->image) }}') center center; background-size: cover;" >
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    @endif

    @include('web.home.introduce')

    @include('web.home.pricing')

    @if($testimonial && count($testimonial->sliders))
        <!-- Professor -->
        <div class="text-center">
            <p class="font-VLBoosterNextFYBlack text-uppercase color-blue-main mx-auto text-khachhangnoigivewowpik text--fontsize-responsive">
                {{  Setting::get('home_testimonial_title') }}
            </p>
        </div>
        <!-- Carousel khach hàng nói gì về wowpik -->
        <div class="d-flex bg-transparent">
            <button class="previousButton-client button--next-previous-clients">
                <img
                        src="/assets/images/left-arrow.png"
                        class="image--logo-style"
                        alt=""
                        srcset=""
                />
            </button>

            <div
                    class="multiple-clients carousel py-5" style="width: 90%;"
                    id="carouselExampleControls"
                    data-ride="carousel"
            >
                @foreach($testimonial->sliders as $slide)
                    <div class="text-center professor-area">
                        <img
                                data-lazy="{{ url($slide->image) }}"
                                data-src="{{ url($slide->image) }}"
                                alt=""
                                srcset=""
                                class="carousel-clients-index mx-auto"
                        />
                        <p class="text-khachhangnoive color-blue-main mt-3">
                            {{ $slide->text }}
                        </p>
                        <p class="text-khachhangnoive color-blue-main mt-3">
                            {{ $slide->name }}
                        </p>
                    </div>

                @endforeach
            </div>
            <button class="nextButton-client button--next-previous-clients">
                <img
                        src="/assets/images/right-arrow.png"
                        class="image--logo-style"
                        alt=""
                        srcset=""
                />
            </button>
        </div>
    @endif

    @if($rows && count($rows))
        <!-- Acitivty -->
        <div class="activity-area text-center my-3">
            <p class="font-VLBoosterNextFYBlack color-blue-main text--fontsize-responsive"
               >{{  Setting::get('home_activities_title') }}</p>
            <!-- Grid -->

            <div class="container">
                @foreach($rows as $row)
                    <div class="row  pb-3">
                        @foreach($row as $item)
                            @if($item->type == 1)
                                <div class="col" style="padding: 0 8px !important;">
                                    <img
                                            data-lazy="{{ url($item->image) }}"
                                            data-src="{{ url($item->image) }}"
                                            class="w-100 h-100 lazy"
                                            alt=""
                                            srcset=""
                                    />
                                </div>
                            @else
                                <div class="col-8 mw-100" style="padding: 0 8px !important;">
                                    <iframe
                                            width="100%"
                                            height="100%"
                                            src="{{ $item->link }}"
                                            frameborder="0"
                                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen
                                    ></iframe>
                                </div>
                            @endif
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    @endif

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ url('js/bootstrap/bootstrap.min.js') }}"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>

    <!-- SLICK -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.plugins.min.js"></script>

    <script type="text/javascript">
        $('.lazy').Lazy();

        $('.multiple-clients').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplaySpeed: 3000,
            autoplay: true,
            prevArrow: $('.previousButton-client'),
            nextArrow: $('.nextButton-client'),
            lazyLoad: 'ondemand',
        });

        $('.multiple-items-toys').slick({
            autoplay: true,
            autoplaySpeed: 3000,
            infinite: true,
            slidesToShow: 5,
            slidesToScroll: 1,
            prevArrow: $('.previousButton-toys'),
            nextArrow: $('.nextButton-toys'),
            asNavFor:'.multiple-items',
            centerMode: true,
            responsive: [
                {
                    breakpoint: 1100,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                    },
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    },
                },
            ],
            lazyLoad: 'ondemand',
        })

        $('.multiple-items').slick({
            autoplay: true,
            autoplaySpeed: 3000,
            infinite: true,
            slidesToShow: 7,
            slidesToScroll: 1,
            prevArrow: $('.previousButton-brand'),
            nextArrow: $('.nextButton-brand'),
            asNavFor:'.multiple-items-toys',
            centerMode: true,
            responsive: [
                {
                    breakpoint: 1250,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1,
                        infinite: true,
                    },
                },
                {
                    breakpoint: 950,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    },
                },{
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    },
                },
            ],
            lazyLoad: 'ondemand',
        })



    </script>

@endsection
