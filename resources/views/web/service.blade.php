@extends('web.layout.1-column')

@section('title', 'Dịch vụ')

@section('content')
<!-- Banner  -->
@if($slider && count($slider->sliders))
<div id="carousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators mx-0">
    @php $count = 0 @endphp
    @for($i = 0; $i < count($slider->sliders); $i++)
      <li data-target="#carousel" data-slide-to="{{ $i }}" class="{{ $i == 0 ? 'active' : '' }}"></li>
      @php $count++; @endphp
      @endfor
  </ol>
  <div class="carousel-inner">
    @php $count = 0 @endphp
    @foreach($slider->sliders as $slide)
    <div class="carousel-item {{ $count == 0 ? 'active' : '' }}">
      <img class="d-block w-100 carousel-images" src="{{ url($slide->image) }}" />
      <div class="khampha-banner">
        @if($slide->text)
        <p class="text-index-banner text-center font-VLBoosterNextFYBlack">
          {{ $slide->text }}
        </p>
        @endif
        @if($slide->button_display && $slide->button_text)
          <div class="btn text-uppercase btn--khampha-banner mx-auto">
            <a href="{{ $slide->link ? $slide->link : '#' }}" class="text-white">{{ $slide->button_text }}</a>
          </div>
        @endif
      </div>
    </div>
    @php $count++; @endphp
    @endforeach
  </div>
</div>
@endif

<!--  UPDATE TEXT  -->
<div class="d-flex justify-content-center" style="background-color: #2c4666;">
  <p class="my-auto" style="color: white; padding: 0.5rem;">
    Miến phí vận chuyển và lắp đặt tại Hà Nội
  </p>
</div>

@if($steps_items && count($steps_items))
<div class="text-center mb-5 three-steps pb-2" style="background-color: rgb(245, 245, 245);">
  <p class="service-contact-title font-VLBoosterNextFYBlack">03 bước đơn giản để nhận bất ngờ</p>
  <!-- Contact circle -->
  <div class="container">
    <div class="row">
      @foreach($steps_items as $item)
      <div class="col-sm">
        <div class="service-contact-circle mx-auto">
          <img src="{{ url($item->image) }}" style="height: 100%;vertical-align: middle; width:50%" alt="" srcset="">
        </div>
        <p class="text-uppercase">
          {{ $item->name }}
        </p>
      </div>
      @endforeach
    </div>
  </div>
</div>
@endif
<!-- Contact  -->

@if($reward_items && count($reward_items))
<!-- Gift -->
<!-- REVIEW: di chuyeern h4, p tuwf container dưới lên trên -->
<div class="text-center pb-5">
  <h4 class="font-VLBoosterNextFYBlack text--fontsize-responsive" style="color: rgb(230, 90, 37);">
    {{ Setting::get('service_rewards_title') }}
  </h4>
  <p class="w-75 mx-auto pb-5" style="font-size: 1.5rem;">
    {!! Setting::get('service_rewards_short_desc') !!}
  </p>

  <div class="container">
    <div class="row">
      @foreach($reward_items as $item)
      <div class="col-sm service-gift-margin-under575px">
        <img src="{{ url($item->image) }}" class="service-contact-logo" alt="" srcset="" />
        @switch($item->sub_type)
        @case(\App\Models\ServiceItem::SUB_TYPE_REWARD_1)
        <img src="/assets/images/service/=.png" class="service-contact-logo-absolute" alt="" srcset="" />
        @break
        @case(\App\Models\ServiceItem::SUB_TYPE_REWARD_2)
        <img src="/assets/images/service/+.png" class="service-contact-logo-absolute" alt="" srcset="" />
        @break
        @case(\App\Models\ServiceItem::SUB_TYPE_REWARD_3)

        @break
        @default
        @break
        @endswitch
        <h4 class="text-uppercase w-75 mx-auto  color-blue-main font-weight-bold">{{ $item->name }}</h4>
        <p class="w-75 mx-auto color-blue-main">
          {{ $item->text }}
        </p>
      </div>
      @endforeach
    </div>
  </div>
  <!--  <div class="container">
                <div class="row">
                    @foreach($reward_items as  $item)
                        <div class="col-sm">
                            <h4 class="text-uppercase w-75 mx-auto  color-blue-main font-weight-bold">{{ $item->name }}</h4>
                            <p class="text-uppercase w-75 mx-auto color-blue-main">
                                {{ $item->text }}
                            </p>
                        </div>
                    @endforeach
                </div>
            </div> -->
</div>
@endif

<!-- Register Now -->
<div style="position: relative;" id="blockRegister">
  <div class="position-relative block--dangky">
    <img src="/assets/images/service02/background-cloud.png" alt="" srcset="" class="w-100 h-100" />
    <img src="/assets/images/service02/ba-em-be.png" class="img-3embe" alt="" srcset="">

  </div> <!-- Button register -->
  <div class="text-center register-now-area">
    <div>
      <h4 class="text-uppercase register-now-title font-VLBoosterNextFYBlack">
        Đăng ký ngay
      </h4>
    </div>
    <!-- Đagư ký cho con -->
    <div>
      <button class="register-now-button text-center text-white text-uppercase" onclick="dangKyChoConButton()"
        id="dangKyChoCon">
        đăng ký cho con
      </button>

    </div>

    <!-- đăng ký quà tặng -->
    <div>

      <button class="register-now-button text-center text-white text-uppercase" onclick="dangKyQuaTangButton()"
        id="dangKyQuaTang">
        đăng ký quà tặng
      </button>

    </div>
  </div>
  <!--  Free -->
  <div class="service-free-absolute">
    <div class="d-none d-xl-flex" style="position: relative ; z-index: 2;">
      <div class="service-register-free text-center mr-4 bg-white" style="z-index: 2;">
        <p class="d-inline-block my-auto" style=" color: rgb(61,96,142);">Miễn phí vận chuyển
        </p>
      </div>
      <div class="service-register-free text-center mr-4 bg-white" style="z-index: 2;">
        <p class="d-inline-block my-auto" style=" color: rgb(61,96,142);">Miễn phí pin</p>
      </div>
      <div class="service-register-free text-center mr-4 bg-white" style="z-index: 2;">
        <p class="d-inline-block my-auto" style=" color: rgb(61,96,142);">Miễn phí tư vấn</p>
      </div>
      <div class="hr hr-mienphi d-none d-xl-block"></div>
    </div>
  </div>
</div>

@include('web.service.forms')

<!-- Take care wowpik toys -->
@if($preservation_items && count($preservation_items))
<div class="service-takecare-toys text-center">
  <h3 class="py-5 text-white font-VLBoosterNextFYBlack" style="font-size: 3vw;">
    {{ Setting::get('service_preservations_title') }}
  </h3>
  <div class="takecare-wowpik-area pb-4">
    @foreach(\App\Models\ServiceItem::getFrontSubTypes() as $key => $type)

    @if(count($preservation_items[$key]))
    <div class="row">
      <div class="col-sm text-uppercase takecare-1st-column">
        {{ $type }}
      </div>
      @foreach($preservation_items[$key] as $item)
      <div class="col-sm mb-5">
        <div class="service-takecare-toys-circle">
          <img src="{{ url($item->image) }}" style="width: 100%;" alt="" srcset="" />
        </div>
        <h5 class="service-takecare-circle-text">
          {{ $item->name }}
        </h5>
        <p class="service-takecare-circle-text">
          {{ $item->text }}
        </p>

      </div>
      @endforeach
      @if(count($preservation_items[$key]) < 3) @for($i=0; $i < 3 - count($preservation_items[$key]);$i++) <div
        class="col-sm">
    </div>
    @endfor
    @endif
  </div>
  @endif
  @endforeach
</div>
</div>
@endif

<div class="" style="
        background: url({{ Setting::get('service_expert_background') }}) no-repeat
          center top;
        height: 60vh;
      "></div>
<div class="promotion-circle">
  <span id="promotion-circle-text" class="align-items-center">
    {{ Setting::get('service_experts_title') }}
  </span>
</div>

<!-- Promotion  -->
@if($experts_items && count($experts_items))
<div class="py-5 mb-3 introduce-area">
  @php $count = 0; $open = true; $num = 2 @endphp
  @foreach($experts_items as $item)
  @if($count % $num == 0)
  <div class="row">
    @endif
    <div class="promotion-circle-margin introduce-circle">
      <img src="{{ url($item->image) }}" style="width: 100%; height: 100% ; object-fit: cover;" alt="" srcset="" />
    </div>
    <div class="col-sm">
      <h5 class="text-uppercase service-promotion-text">
        {!! $item->name !!}
      </h5>
      <p class="service-promotion-text">
        {{ $item->text }}
      </p>
    </div>
    @if($count % $num == 1)
  </div>
  @endif
  @php $count++ @endphp
  @endforeach
</div>
@endif
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
  integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
  integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
@endsection