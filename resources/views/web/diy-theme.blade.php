@extends('web.layout.1-column')

@section('title', 'Góc chia sẻ')

@section('customs')
    <link rel="stylesheet" href="{{ url('css/diy.css') }}"/>
@endsection

@section('content')
    <div class="container mt-3">
        <div class="row">
            <div class="col">
                <div class="mx-auto my-3">
                    <a class="text-decoration-none text-dark" href="{{ url('') }}">Trang chủ</a>
                    &gt;
                    <a class="text-decoration-none text-dark" href="{{ url('goc-kheo-tay') }}">Góc khéo tay</a>
                </div>
                <h3 class="font-VLBoosterNextFYBlack color-red-main">
                    Hãy cùng khám phá đồ chơi STEAM tại Wowwpik nhé !
                </h3>
            </div>
        </div>
    </div>
    <div class="d-none d-lg-block custom-div container">
        @if(count($themes))
            @php
                $rows = array_chunk($themes->toArray(), 3);
            @endphp
            @foreach($rows as $row)
                <div class="row mt-4 mx-3">
                    <!-- id%3 === 0 -->
                    @if(isset($row[0]))
                        <div class="col-lg-4">
                            <a href="{{ url('goc-kheo-tay/chu-de/'. $row[0]['slug']) }}" class="DIY__buttonArea">
                                <div class="DIY__buttons DIY__buttons-left">
                                    <img src="{{ url($row[0]['image']) }}" class="buttons__image" alt="">
                                    <p class="buttons__text font-VLBoosterNextFYBlack">
                                        {{ $row[0]['title'] }}
                                    </p>
                                </div>
                            </a>
                        </div>
                    @endif
                <!-- id%3 === 1 -->
                    @if(isset($row[1]))
                        <div class="col-lg-4">
                            <a href="{{ url('goc-kheo-tay/chu-de/'. $row[1]['slug']) }}" class="DIY__buttonArea">
                                <div class="DIY__buttons DIY__buttons-center">
                                    <img src="{{ url($row[1]['image']) }}" class="buttons__image" alt="">
                                    <p class="buttons__text font-VLBoosterNextFYBlack">
                                        {{ $row[1]['title'] }}
                                    </p>
                                </div>
                            </a>
                        </div>
                    @endif
                <!-- id%3 === 2 -->
                    @if(isset($row[2]))
                        <div class="col-lg-4">
                            <a href="{{ url('goc-kheo-tay/chu-de/'. $row[2]['slug']) }}" class="DIY__buttonArea">
                                <div class="DIY__buttons DIY__buttons-right">
                                    <img src="{{ url($row[2]['image']) }}" class="buttons__image" alt="">
                                    <p class="buttons__text font-VLBoosterNextFYBlack">
                                        {{ $row[2]['title'] }}
                                    </p>
                                </div>
                            </a>
                        </div>
                    @endif
                </div>
            @endforeach
        @endif
    </div>

    <div class="d-block d-lg-none">
        @if(count($themes))
            @php
                $rows = array_chunk($themes->toArray(), 2);
            @endphp
            @foreach($rows as $row)
        <div class="row mt-4 mx-3">
        <!-- id % 2 === 0 -->
        <div class="col-5 offset-1 ">
        @if(isset($row[0]))
            <a href="{{ url('goc-kheo-tay/chu-de/'. $row[0]['slug']) }}" class="DIY__buttonArea">
            <div class="DIY__buttons">
                <img src="{{ url($row[0]['image']) }}"  class="buttons__image" alt="">
                <p class="buttons__text font-VLBoosterNextFYBlack">
                {{ $row[0]['title'] }}
                </p>
            </div>
            </a>
        </div>
        @endif
        <!-- id % 2 === 1 -->
        @if(isset($row[1]))
        <div class="col-5">
            <a  href="{{ url('goc-kheo-tay/chu-de/'. $row[1]['slug']) }}" class="DIY__buttonArea">
            <div class="DIY__buttons ">
                <img src="{{ url($row[1]['image']) }}" class="buttons__image" alt="">
                <p class="buttons__text font-VLBoosterNextFYBlack">
                {{ $row[0]['title'] }}
                </p>
            </div>
            </a>
        </div>
        @endif
        </div>
        @endforeach
        @endif
    </div>
@endsection
