<!-- Pricing -->
<div class="text-center pt-4" style="background-color: #f1f1f2;">
    <!-- Title khám phá + free -->
    <div class="container mb-5">
        <div class="row">
            <div class="col-12 col-lg-2"></div>
            <div class="col-12 col-lg-9">
                <h2 class="d-block text-uppercase font-VLBoosterNextFYBlack color-red-main khampha-batngo-text text--fontsize-responsive">
                    {!! Setting::get('home_pricing_title') !!}
                </h2>
                <div class="row" style="position: relative;">
                    <!-- Free section -->
                    <div class="col free-button-media"
                         style="z-index: 2;">
                        <p class="background-green free-section-index">
                            Miễn phí vận chuyển
                        </p>
                    </div>
                    <div class="col free-button-media"
                         style="z-index: 2;">
                        <p class="background-green free-section-index">Miễn phí pin</p>
                    </div>
                    <div class="col free-button-media"
                         style="z-index: 2;">
                        <p class="background-green free-section-index">
                            Miễn phí tư vấn
                        </p>
                    </div>
                    <div class="hr hr-mienphi d-none d-lg-block"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Card -->
    <div class="container pb-5">
        <div class="row">
            <div class="col-12 col-lg-2"></div>
            <div class="col-12 col-lg-3 text-center">
                <div class="card discovery-surprise">
                    <img
                            class="card-img-top"
                            src="{{ url('assets/images/KhamPha/yay.jpg') }}"
                            alt="Card image cap"
                    />
                    <div class="card-body">
                        <h5 class="card-title font-VLBoosterNextFYBlack color-green-main yyw-font">
                            YAY!
                        </h5>
                        <p class="card-text color-blue-main pricing font-weight-bold mb-0 d-none d-md-block">
                            1.200.000đ
                            <span class="absolute-text-right text-dark font-weight-normal">
                                Chi phí
                            </span>
                        </p>
                        <p class="card-text color-blue-main pricing text-right text-md-center">
                            01 tháng
                            <span class="absolute-text-right text-dark"> Thời hạn </span>
                        </p>
                        <hr class="d-none d-md-block"/>
                        <div class="d-none d-md-block">
                            <p class="pb-1 media-text-right color-blue-main">
                                05
                                <span class="absolute-text-right text-dark">
                                  Truy cập kho DIY
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-green circle-margin-custom"
                            >
                                <span class="absolute-text-right">
                                  Hội viên Wowpik
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-green circle-margin-custom"
                            >
                                <span class="absolute-text-right">
                                  Dồ chơi chính hãng
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-green circle-margin-custom"
                            >
                                <span class="absolute-text-right">
                                  Tham gia sự kiện
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-green-opacity circle-margin-custom"
                            >
                                <span class="absolute-text-right">
                                  Gặp gỡ chuyên gia
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-green-opacity circle-margin-custom"
                            >
                                <span class="absolute-text-right">
                                  Quà tặng sinh nhật
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-green-opacity circle-margin-custom"
                            >
                                <span class="absolute-text-right">
                                  Tư vẫn miễn phí
                                </span>
                            </p>
                            <p class="media-text-right">
                                8.000.000đ ++
                                <span class="absolute-text-right">
                                  Tương đương giá trị nhận được mỗi tháng
                                </span>
                            </p>
                        </div>

                        <a
                                href="{{ url('dich-vu?option=yay') }}"
                                class="btn btn--dangky-ngay1 background-green mt-4"
                                style="color: white;"
                        >Đăng ký ngay</a
                        >
                    </div>
                </div>
                <!-- Card Above -->
            </div>
            <div class="col-12 col-lg-3">
                <div class="card discovery-surprise">
                    <img
                            class="card-img-top"
                            src="{{ url('assets/images/KhamPha/yeah.jpg') }}"
                            alt="Card image cap"
                    />
                    <div class="card-body">
                        <h5
                                class="card-title font-VLBoosterNextFYBlack color-blue-main yyw-font"
                        >
                            YEAH!
                        </h5>
                        <p class="card-text color-blue-main pricing font-weight-bold mb-0 d-none d-md-block">
                            990.000đ
                            <span
                                    class="absolute-text-right d-lg-none text-dark font-weight-normal"
                            >
                                Chi phí
                              </span>
                        </p>
                        <p class="card-text color-blue-main pricing text-right text-md-center">
                            06 tháng
                            <span class="absolute-text-right d-lg-none text-dark">
                                Thời hạn
                              </span>
                        </p>
                        <hr class="d-none d-md-block"/>

                        <div class="d-none d-md-block">
                            <p class="pb-1 media-text-right color-blue-main">
                                20<span class="absolute-text-right d-lg-none">
                                  Truy cập kho DIY
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-blue circle-margin-custom"
                            >
                                <span class="absolute-text-right d-lg-none">
                                  Hội viên Wowpik
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-blue circle-margin-custom"
                            >
                                <span class="absolute-text-right d-lg-none">
                                  Dồ chơi chính hãng
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-blue circle-margin-custom"
                            >
                                <span class="absolute-text-right d-lg-none">
                                  Tham gia sự kiện
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-blue circle-margin-custom"
                            >
                                <span class="absolute-text-right d-lg-none">
                                  Gặp gỡ chuyên gia
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-blue-opacity circle-margin-custom"
                            >
                                <span class="absolute-text-right d-lg-none">
                                  Quà tặng sinh nhật
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-blue-opacity circle-margin-custom"
                            >
                                <span class="absolute-text-right d-lg-none">
                                  Tư vẫn miễn phí
                                </span>
                            </p>
                            <p class="media-text-right">
                                30.000.000đ ++
                                <span class="absolute-text-right d-lg-none">
                                  Tương đương giá trị nhận được mỗi tháng
                                </span>
                            </p>
                        </div>

                        <a
                                href="{{ url('dich-vu?option=yeah') }}"
                                class="btn btn--dangky-ngay2 background-blue mt-4"
                                style="color: white;"
                        >Đăng ký ngay</a
                        >
                    </div>
                </div>
                <!-- CardAbove -->
            </div>
            <div class="col-12 col-lg-3">
                <div>
                    <div class="background-blue p-2 rounded-circle best-solution" >
                        <div class="border w-100 h-100 d-flex justify-content-center align-items-center rounded-circle">
                            <p class="text-white m-0">Lựa chọn <br> tốt nhất</p>
                        </div>
                    </div>
                </div>
                <div class="card discovery-surprise">
                    <img
                            class="card-img-top"
                            src="{{ url('assets/images/KhamPha/wow.jpg') }}"
                            alt="Card image cap"
                    />
                    <div class="card-body">
                        <h5
                                class="card-title font-VLBoosterNextFYBlack color-red-main yyw-font"
                        >
                            WOW!
                        </h5>
                        <p class="card-text color-blue-main pricing font-weight-bold mb-0 d-none d-md-block">
                            790.000đ
                            <span
                                    class="absolute-text-right d-lg-none text-dark font-weight-normal"
                            >
                                Chi phí
                              </span>
                        </p>
                        <p class="card-text color-blue-main pricing text-right text-md-center">
                            12 tháng
                            <span class="absolute-text-right d-lg-none text-dark">
                                Thời hạn
                              </span>
                        </p>
                        <hr class="d-none d-md-block"/>
                        <div class="d-none d-md-block">
                            <p
                                    class="pb-1 media-text-right index-pricing-wow color-blue-main"
                            >
                                Không giới hạn
                                <span class="absolute-text-right d-lg-none text-dark">
                                  Truy cập kho DIY
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-red circle-margin-custom"
                            >
                                <span class="absolute-text-right d-lg-none">
                                  Hội viên Wowpik
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-red circle-margin-custom"
                            >
                                <span class="absolute-text-right d-lg-none">
                                  Dồ chơi chính hãng
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-red circle-margin-custom"
                            >
                                <span class="absolute-text-right d-lg-none">
                                  Tham gia sự kiện
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-red circle-margin-custom"
                            >
                                <span class="absolute-text-right d-lg-none">
                                  Gặp gỡ chuyên gia
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-red circle-margin-custom"
                            >
                                <span class="absolute-text-right d-lg-none">
                                  Quà tặng sinh nhật
                                </span>
                            </p>
                            <p
                                    class="circle media-text-right background-red circle-margin-custom"
                            >
                                <span class="absolute-text-right d-lg-none">
                                  Tư vẫn miễn phí
                                </span>
                            </p>
                            <p class="media-text-right">
                                60.000.000đ ++
                                <span class="absolute-text-right d-lg-none">
                                  Giá trị nhận được tương ứng
                                </span>
                            </p>
                        </div>
                        <a
                                href="{{ url('dich-vu?option=wow') }}"
                                class="btn btn--dangky-ngay3 background-red mt-4"
                                style="color: white;"
                        >Đăng ký ngay</a
                        >
                    </div>
                </div>
                <!-- card above -->
            </div>
        </div>
    </div>
    <!-- Quy Trình -->
    <img
            data-src="/assets/images/quy-trinh.png"
            class="process-area lazy"
            alt=""
            srcset=""
    />
</div>
