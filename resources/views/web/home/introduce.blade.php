<div class=" gia-tri-nhan-duoc color-245x3 mx-auto">
    <p class="text-uppercase font-VLBoosterNextFYBlack text-center text-white gia-tri-title text--fontsize-responsive">
        Giá trị nhận được khi tham gia WOWPIK
    </p>
    <div class="row mx-auto mb-5" style="width:95%">
        <div class="col-12 col-lg-2 text-center">
            <img src="/assets/images/KhamPha/yeah.jpg" class="image--gia-tri-nhan-duoc" style="border-radius: 50%;" alt="" srcset="">
        </div>
        <div class="col-12 col-lg-4 mt-3 text--center-responsive">
            <p class="btn background-red gia-tri-card-title text-white text-uppercase"> Đối với bé</p>
            <p class=" text-white gia-tri-card-text doivoi--margin">
                Thiết kế riêng cho mỗi tháng tuổi của bé </br>
                Đồ chơi an toàn, nhập khẩu chính hãng</br>
                Trải nghiệm đa dạng, hứng thú hàng tháng</br>
                Gắn kết gia đình.
            </p>
        </div>
        <div class="col-12 col-lg-2 text-center">
            <img src="/assets/images/index-card-02.png" class="image--gia-tri-nhan-duoc" style="border-radius: 50%;" alt="" srcset="">

        </div>

        <div class="col-12 col-lg-4 mt-3 text--center-responsive">
            <p class="btn background-red gia-tri-card-title text-white text-uppercase"> Đối với ba mẹ</p>
            <p class=" text-white gia-tri-card-text doivoi--margin">
                Tiết kiệm thời gian tìm đồ chơi phù hợp. <br/>
                Tiết kiệm không gian lưu trữ đồ cũ.<br/>
                Tài chính tối ưu, tiêu dùng thông minh.
            </p>
        </div>
    </div>
    <p class="text-uppercase font-VLBoosterNextFYBlack text-center text-white text--fontsize-responsive my-3  text--cungwowpikgiupthegioi">
        CÙNG WOWPIK GIÚP THẾ GIỚI TỐT ĐẸP HƠN
    </p>


    <div class="pb-5" style="width:80% ;margin: auto;">
        <div class="row">
            <div class="col-12 col-lg-4 mx-auto text-center">
                <img data-src="./assets/images/svg/Asset1.svg" class="lazy" style="height: 6rem;  margin:auto ; display:block;margin-bottom: 1rem;" alt="" srcset="">
                <p class="btn background-green cung-wowpik-button text-center"> TRÁCH NHIỆM XÃ HỘI </p>
                <p class="text-white cung-wowpik-text mx-auto">
                    Hạn chế lãng phí xã hội
                </p>
                <p class="text-white cung-wowpik-text mx-auto">

                    Giảm thiểu lãng phí về thời gian và không gian
                </p>
                <p class="text-white cung-wowpik-text mx-auto">

                    Cải thiện chất lượng cuộc sống, nâng cao giáo dục và phát triển cho trẻ nhỏ.
                </p>
            </div>
            <div class="col-12 col-lg-4 mx-auto text-center">
                <img data-src="./assets/images/svg/Asset2.svg" class="lazy" style="height: 6rem;  margin:auto ; display:block;margin-bottom: 1rem;" alt="" srcset="">
                <p class="btn background-green cung-wowpik-button text-center"> SỐNG XANH HỮU ÍCH </p>
                <p class="text-white cung-wowpik-text mx-auto">Góp phần bảo vệ và cải thiện tình trạng môi trường qua việc
                    giảm thiểu rác thải, vỏ hộp, bao bì.</p>
                <p class="text-white cung-wowpik-text mx-auto">
                    Gần gũi với môi trường hơn khi bé được tiếp xúc với những món đồ chơi DIY - tự làm từ những vật liệu tái chế
                    và từ thiên nhiên
                </p>
                <p class="text-white cung-wowpik-text mx-auto">
                    Sáng tạo cùng bé từ những vận dụng không dùng tới - tăng cao tính tái Sử dụng
                </p>
            </div>
            <div class="col-12 col-lg-4 mx-auto text-center">
                <img data-src="./assets/images/svg/Asset3.svg" class="lazy" style="height: 6rem;  margin:auto ; display:block;margin-bottom: 1rem;" alt="" srcset="">
                <p class="btn background-green cung-wowpik-button text-center"> CHIA SẺ CỘNG ĐỒNG </p>
                <p class="text-white cung-wowpik-text mx-auto">
                    Đóng góp một phần cho các chương trình thiện nguyện có ý nghĩa
                    to lớn với
                    cộng đồng.
                </p>
                <p class="text-white cung-wowpik-text mx-auto">
                    Chung tay giúp đỡ những hoàn cảnh khó khăn trong cuộc sống.
                </p>
                <p class="text-white cung-wowpik-text mx-auto">
                    Chia sẻ đồ chơi tới các em bé còn thiếu thốn.
                </p>
                <p class="text-white cung-wowpik-text mx-auto">
                    Ủng hộ một phần lợi nhuận tới các tổ chức/quỹ vì trẻ em.</p>
            </div>
        </div>
    </div>
</div>
