<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Bootstrap CSS -->
    <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
            integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
            crossorigin="anonymous"
    />
    <link rel="shortcut icon" type="image/png" href="{{ url('images/favicon-02.svg') }}"/>

    <!-- FontAWESOME -->
        <!-- Thay CDN -->

   <!--  <link
            href="./fontawesome-free-5.13.0-web/css/fontawesome.css"
            rel="stylesheet"
    />
    <link
            href="./fontawesome-free-5.13.0-web/css/brands.css"
            rel="stylesheet"
    />
    <link href="./fontawesome-free-5.13.0-web/css/solid.css" rel="stylesheet" />
    <script defer src="./fontawesome-free-5.13.0-web/js/brands.js"></script>
    <script defer src="./fontawesome-free-5.13.0-web/js/solid.js"></script>
    <script
            defer
            src="./fontawesome-free-5.13.0-web/js/fontawesome.js"
    ></script> -->
    <script src="https://kit.fontawesome.com/39619ca06c.js" crossorigin="anonymous"></script>

    <!-- Slick Css -->
    <!-- Add the slick-theme.css if you want default styling -->
    <link
            rel="stylesheet"
            type="text/css"
            href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"
    />
    <!-- Add the slick-theme.css if you want default styling -->
    <link
            rel="stylesheet"
            type="text/css"
            href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"
    />
    <script
            type="text/javascript"
            src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"
    ></script>
    <!-- CustomCSS -->
    <link rel="stylesheet" href="css/product.css" />
    <link rel="stylesheet" href="css/style.css" />

    <title>Sản phẩm</title>
</head>
