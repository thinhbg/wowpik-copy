<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- BootStrap -->
    <link rel="stylesheet" href="{{ url('css/bootstrap/bootstrap.min.css') }}" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- BootStrap -->
    <link rel="shortcut icon" type="image/png" href="{{ url('assets/images/favicon-02.svg') }}"/>

    <!-- FontAwesome -->
    <!-- <script src="{{ url('js/fontawesome/file.js') }}" crossorigin="anonymous"></script> -->

    <script src="https://kit.fontawesome.com/39619ca06c.js" crossorigin="anonymous"></script>

    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet"  type="text/css" href="{{ url('css/slick/slick.css') }}"/>
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet"  type="text/css" href="{{ url('css/slick/slick-theme.css') }}"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.js"></script>
    <script type="text/javascript" src="{{ url('js/slick/slick.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/jquery.sticky.js') }}"></script>
    @yield('customs')
    <link rel="stylesheet" href="{{ url('css/style.css') }}" />
    <title>@yield('title')</title>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-167878023-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-167878023-1');
    </script>

    <script>
        $(document).ready(function () {
            $(".navbar").sticky({ topSpacing: 0, zIndex:2});
            // $(".navbar").css('z-index', 1)
        })
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <!-- Toastr -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
</head>
