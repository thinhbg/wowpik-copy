<!-- Footer -->
<footer class="d-block d-lg-none mt-5">
  <div class="row mb-3">
    <div class="col-lg-2 col-12 footer-logo-middle">
      <a class="active bg-white" href="#">
        <img src="./assets/images/logo.png" alt="" srcset="" />
      </a>
    </div>
</footer>

<nav class="navbar-light d-lg-flex d-none mt-3" style="
padding: 0.5rem 1rem;">
  <div style="width: 10rem;">
    <a class="navbar-brand" href="index.html">
      <img src="./assets/images/logo.png" alt="logo" srcset="" id="logo-style" />
    </a>
  </div>
  <div class="row flex-grow-1">
    <div class="col-12 col-md-3">
      <ul class="list-style-none">
        <li><a href="#" class="text-dark">Về Wowpik</a></li>
        <li><a href="#" class="text-dark">Vì sao chọn Wowpik</a></li>
        <li><a href="#" class="text-dark">Tin tức</a></li>
        <li><a href="#" class="text-dark">Liên hệ</a></li>
        <li><a href="#" class="text-dark">Nghề nghiệp</a></li>
      </ul>
    </div>
    <div class="col-12 col-md-3">
      <ul class="list-style-none">
        @php
          $ranges = \App\Models\TProductAge::all();
        @endphp
        @foreach($ranges as $range)
          <li><a href="{{ url('do-choi?age_range_id=' . $range->id) }}" class="text-dark">{{ $range->name }}</a></li>
        @endforeach
        <li><a href="{{ url('goc-kheo-tay') }}" class="text-dark">DIY</a></li>
      </ul>
    </div>
    <div class="col-12 col-md-3">
      <ul class="list-style-none">
        <li><a href="{{ url('tin-tuc/bao-mat') }}" class="size--09rem text-dark">Điều khoản bảo mật </a></li>
        <li><a href="{{ url('tin-tuc/noi-quy') }}" class="size--09rem text-dark">Nội Quy </a></li>
        <li><a href="{{ url('tin-tuc/van-chuyen') }}" class="size--09rem text-dark">Vận chuyển </a></li>
        <li><a href="#" class="text-dark">Site map </a></li>
        <li><a href="#" class="text-dark">Hỏi đáp </a></li>
        <li><a href="#" class="text-dark">Hỗ trợ </a></li>
      </ul>
    </div>
    <div class="col-12 col-md-3 d-none d-md-block">
      <form action="{{ url('theo-doi') }}" method="POST" style="text-align: right; display:flex; margin-bottom: 0.5rem;">
          @csrf
          <input type="hidden" name="referer" value="{{ url()->current() }}">
          <input type="email" name="email" placeholder="Vui lòng nhập email của bạn"  style="flex-grow:1;border: none;"/>
          <button type="submit" style="
            cursor: pointer;
            box-shadow: 1px 1px 2px  rgba(0,0,0,0.75);
            border: none;
            background: white;
            border-radius: 5px;
          ">Theo dõi</button>
      </form>
      <div class="ml-auto" style="
        width: 100%;
        border-bottom: 2px solid black;"></div>
      <div class="ml-auto" style="width: 100%; font-size: 12px; text-align: justify">
        <br />
        Wowpik được vận hành bởi Công ty Cổ phần Maitoys Quốc tế Địa chỉ: Phòng 904, Tầng 9, Tòa nhà văn phòng 3A, Ngõ
        82, Phố Duy Tân, Phường Dịch Vọng Hậu, Quận Cầu Giấy, Thành phố Hà Nội. Đại diện theo pháp luật: Phạm Hương Thảo
        MST 0108431788 Hotline: 093 686 9669
      </div>
    </div>
  </div>
</nav>

<div class="float-buttons">
  <div class="d-flex flex-column ">
    <div class="mb-3  rounded-circle float--button-box-shadow">
      <a href="https://m.me/WOWPIK">
        <i class="fab fa-facebook-messenger messenger-float-icon fa-2x " aria-hidden="true"></i>
      </a>
    </div>
    <div class=" mb-3  rounded-circle float--button-box-shadow">
      <a href="tel:0936869669">
        <i class="fas fa-phone fa-2x" aria-hidden="true"></i>
      </a>
    </div>
    <div class="rounded-circle float--button-box-shadow">
      <a href="http://zalo.me/0936869669">
        <img src="{{ url('assets/images/svg/zalo.svg') }}" class="zalo-svg" alt="">
      </a>
    </div>
  </div>
</div>


<script type="text/javascript">
$(document).ready(function() {
  @if($message = Session::get('success'))
  toastr.success('{{ $message }}')
  @endif


  @if($message = Session::get('error'))
  toastr.error('{{ $message }}')
  @endif


  @if($message = Session::get('warning'))
  toastr.warning('{{ $message }}')
  @endif


  @if($message = Session::get('info'))
  toastr.info('{{ $message }}')
  @endif
});
</script>
