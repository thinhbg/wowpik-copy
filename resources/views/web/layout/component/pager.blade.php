@if($collection->hasPages())
    <nav aria-label="Page navigation">
        <ul class="pagination justify-content-end">
            @if ($collection->onFirstPage())
                <li class="page-item">
                    <a class="page-link disabled" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="{{ $collection->previousPageUrl() }}" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
            @endif

            @php $currentPage = $collection->currentPage() @endphp

                {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)

                            @if($page == $currentPage)
                                <li class="page-item active"><a class="page-link">{{ $page }}</a></li>
                            @elseif($currentPage == 1)

                                @if($page == 2 || $page == 3)
                                    <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                                @endif

                            @elseif(!$collection->hasMorePages())
                                @if($page == $currentPage - 1 || $page == $currentPage - 2)
                                    <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                                @endif
                            @else
                                @if($page == $currentPage - 1 || $page == $currentPage + 1)
                                    <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                                @endif
                            @endif
{{--                        @if($page == $currentPage && $collection->onFirstPage())--}}
{{--                            @if ($page == $paginator->currentPage())--}}
{{--                                <li class="page-item active"><a class="page-link">{{ $page }}</a></li>--}}
{{--                            @elseif($page == $currentPage + 1 || $page == $currentPage + 1)--}}
{{--                                <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>--}}
{{--                            @endif--}}
{{--                        @elseif($page == $currentPage && !$collection->hasMorePages())--}}
{{--                            @if ($page == $currentPage)--}}
{{--                                <li class="page-item active"><a class="page-link">{{ $page }}</a></li>--}}
{{--                            @elseif($page == $currentPage - 1 || $page == $currentPage -2)--}}
{{--                                <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>--}}
{{--                            @endif--}}
{{--                        @else--}}
{{--                            @if ($page == $currentPage)--}}
{{--                                <li class="page-item active"><a class="page-link">{{ $page }}</a></li>--}}
{{--                            @elseif($page == $currentPage - 1 || $page == $currentPage + 1)--}}
{{--                                <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>--}}
{{--                            @endif--}}
{{--                        @endif--}}
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($collection->hasMorePages())
                <li class="page-item">
                    <a class="page-link" href="{{ $collection->nextPageUrl() }}" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            @else
                <li class="page-item disabled">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            @endif
        </ul>
    </nav>
@endif