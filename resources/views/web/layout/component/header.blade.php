<nav class="navbar-expand-lg navbar-light bg-light" style="
display: flex;
flex-wrap: wrap;
justify-content: space-between;
  position: fixed;
    top: 0;
    left: 0;
    right: 0;
    height: 82px;
    z-index: 2;">
  <a class="navbar-brand" href="{{ url('/') }}" style="margin: 0.5rem 1rem;">
    <img src="{{ url('assets/images/logo.png') }}" alt="logo" class="mw-100 " srcset="" id="logo-style" />
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse bg-light" id="navbarSupportedContent" style="margin-right: 1rem">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link text-uppercase navbar-text navbar-text-color margin-left-custom" style="width: 9rem;"
          href="{{ url('dich-vu') }}">Dịch vụ</a>
      </li>
      <li class="nav-item">
        <div class="dropdown">
          <a class="text--hover-bold text-uppercase navbar-text navbar-text-color margin-left-custom item--navbar-size"
            href="{{ url('do-choi') }}">đồ chơi</a>
          <div class="dropdown-content">
            @php $filters = \App\Models\TProduct::getFilters() @endphp
            <div class="row">
              @foreach($filters as $key => $filter)
              <div class="{{ $filter['menu-class'] }}">
                <div class="w-100 font-weight-bold text-uppercase">
                  {{ $filter['label'] }} </div>
                @foreach($filter['options'] as $key => $option)
                <a class="{{ isset($filter['item-class']) ? $filter['item-class'] : ''  }}"
                  href="{{ url("do-choi?{$filter['key']}=$key") }}">{{ $option }} </a>
                @endforeach
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </li>
      {{--            <li class="nav-item">--}}
      {{--                <a--}}
      {{--                        class="nav-link text-uppercase navbar-text navbar-text-color margin-left-custom"--}}
      {{--                        style="width: 9rem;"--}}
      {{--                        href="#"--}}
      {{--                >đồ DIY</a--}}
      {{--                >--}}
      {{--            </li>--}}
      <li class="nav-item">
        <a class="nav-link text-uppercase navbar-text navbar-text-color margin-left-custom" style="width: 9rem;"
          href="{{ url('goc-chia-se') }}">góc chia sẻ</a>
      </li>
    </ul>
    <!-- Thay justifycontent  -->
    {{--        <div class="d-flex justify-content-lg-center justify-content-sm-start align-items-center">--}}
    {{--            <a href="#" class="text-uppercase decoration-background navbar-text">Hội viên</a>--}}
    {{--            <a href="#" class="decoration-background d-none d-lg-block"> <!-- Thêm d-none vs d-lg-block -->--}}
    {{--                <img src="/assets/images/cart.png" alt="" srcset="" />--}}
    {{--            </a>--}}
    {{--            <a href="#" class="decoration-background d-none d-lg-block"> <!-- Thêm d-none vs d-lg-block -->--}}
    {{--                <i class="fas fa-search fa-lg"></i>--}}
    {{--            </a>--}}
    {{--        </div>--}}

    <div class="d-flex justify-content-lg-center justify-content-sm-start  align-items-center">

      @if(auth()->user())
      <a href="{{ url('thong-tin')  }}" class="text-uppercase decoration-background navbar-text">Xin chào
        {{ auth()->user()->email }} </a>
      @else
      <a href="{{ url('dang-nhap')  }}" class="text-uppercase decoration-background navbar-text">Hội viên</a>
      @endif
      <div class="d-none d-lg-flex">
        {{--                <a href="#" class="decoration-background">--}}
        {{--                    <img src="/assets/images/cart.png" alt="" srcset="" />--}}
        {{--                </a>--}}

        <i class="fas fa-search fa-lg searchicon btn" id="search-icon" aria-hidden="true"></i>
        <!-- REVIEW: thêm d-flex, và select -->
        <form id="search-form" action="{{ url('tim-kiem') }}" class='d-flex'>
          <input type="text" name="q" id="search-input" class="rounded-0 bg-light search--input-navbar "
            value="{{ request()->get('q') }}" style="display: {{ request()->get('q') ? 'block' : 'none' }}">
          <select class="rounded-0 search--input-navbar bg-light" id="select-input" name="loai"
            style="display: {{ request()->get('q') ? 'block' : 'none' }}">
            <option value="product" {{ request()->get('loai') == 'product' ? 'selected' : '' }}>Đồ chơi</option>
            <option value="blog" {{ request()->get('loai') == 'blog' ? 'selected' : '' }}>Blog</option>
          </select>
        </form>
      </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function() {
      @if(!request() -> get('q'))
      $("#search-icon").click(function() {
        $('#search-input').slideToggle()
        $('#select-input').slideToggle()
      })
      @endif
    })
    </script>

    {{--        <div class="d-flex justify-content-center align-items-center">--}}
    {{--            <a href="#" class="text-uppercase decoration-background navbar-text"--}}
    {{--            >Hội viên</a--}}
    {{--            >--}}
    {{--            <a href="#" class="decoration-background ">--}}
    {{--                <img src="/assets/images/cart.png" alt="" srcset="" />--}}
    {{--            </a>--}}
    {{--            <a href="#" class="decoration-background ">--}}
    {{--                <i class="fas fa-search fa-lg"></i>--}}
    {{--            </a>--}}
    {{--        </div>--}}


    <!-- 3 icon social network -->
    <!-- REVIEW: em thêm phần ẩn hiển để k bị vỡ khi về mobile -->
    <div class="ml-auto">
      {{--            <div class="d-inline-block d-lg-none">--}}
      {{--                <a href="#" class="decoration-background">--}}
      {{--                    <img src="/assets/images/cart.png" alt="" srcset="" />--}}
      {{--                </a>--}}
      {{--            </div>--}}
      <div class="d-inline-block d-lg-none">
        <div class="d-flex">
          {{--                <a href="#" class="decoration-background">--}}
          {{--                    <i class="fas fa-search fa-lg"></i>--}}
          {{--                </a>--}}
          <form id="search-form" action="{{ url('tim-kiem') }}" class='d-flex'>
            <input type="text" name="q" class="rounded-0 bg-light d-inline-block search--input-navbar "
              value="{{ request()->get('q') }}">
            <select class="rounded-0 d-inline-block search--input-navbar bg-light" name="loai">
              <option value="product" {{ request()->get('loai') == 'product' ? 'selected' : '' }}>Đồ chơi</option>
              <option value="blog" {{ request()->get('loai') == 'blog' ? 'selected' : '' }}>Blog</option>
            </select>
          </form>
        </div>
      </div>
      <div class="d-inline-block">
        <a href="https://m.facebook.com/WOWPIK/?tsid=0.6413269024575472&source=result" target="_blank"
          class="decoration-background-border-radius facebook-hover d-flex justify-content-center align-items-center">
          <i class="fab fa-facebook-f" style="line-height: normal !important;font-size:0.9rem"></i>
        </a>
      </div>
      <div class="d-inline-block">
        <a href="https://instagram.com/wowpik.vn?igshid=89e5i7jwruwu" target="_blank"
          class="decoration-background-border-radius instagram-hover d-flex justify-content-center align-items-center">
          <i class="fab fa-instagram" style="line-height: normal !important;font-size:0.9rem">
          </i>
        </a>
      </div>
      {{--            <div class="d-inline-block">--}}
      {{--                <a href="{{ Setting::get('header_youtube_title') }}"
      class="decoration-background-border-radius youtube-hover d-flex justify-content-center align-items-center">--}}
      {{--                    <i class="fab fa-youtube" style="line-height: normal !important;font-size:0.9rem"></i>--}}
      {{--                </a>--}}
      {{--            </div>--}}
    </div>
  </div>
</nav>

<div style="margin-bottom: 5.125rem"></div>
