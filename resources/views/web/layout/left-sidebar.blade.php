<html>
    @include('web.layout.component.head')
    <body>
        @include('web.layout.component.header')

        @yield('left-sidebar')

        @yield('content')

        @include('web.layout.component.footer')
    </body>
</html>