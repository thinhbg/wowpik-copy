<!DOCTYPE html>
<html lang="en">
    @include('web.layout.component.head')

    <body>
    @include('web.layout.component.header')

    @yield('content')

    @include('web.layout.component.footer')
    </body>
</html>