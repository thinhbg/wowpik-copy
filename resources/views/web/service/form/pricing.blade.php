<div class="text-center pt-4 blockPricingForm">
  <p class="text-uppercase text-left text-black-50" style="width: 90%;">BƯỚC 2 : lựa chọn gói dịch vụ sau
  </p>
  <input type="hidden" value="" name="price-option" />
  <div class=" container" style="padding-bottom: 3rem;">
    <div class="row">
      <div class="d-none d-lg-block col-lg-2"></div>
      <div class="col-4 col-lg-3 goiDichVu-style text-center">
        <div class="card discovery-surprise">
          <img class="card-img-top" src="./assets/images/KhamPha/yay.jpg" alt="Card image cap" />
          <div class="card-body card-body--styling">
            <h5 class="card-title font-VLBoosterNextFYBlack color-green-main yyw-font">
              YAY!
            </h5>
            <p class="card-text color-blue-main pricing font-weight-bold mb-0 d-none d-lg-block">
              1.200.000đ
              <span class="absolute-text-right text-dark font-weight-normal">
                Chi phí
              </span>
            </p>
            <p class="d-none d-lg-block card-text color-blue-main pricing text-right text-md-center">
              01 tháng
              <span class="absolute-text-right text-dark"> Thời hạn </span>
            </p>
            <!-- Hiển thị ở mobile -->
            <p class="d-block d-lg-none color-blue-main">
              Chi phí
              <br>
              1.200.000đ
            </p>
            <p class="d-block d-lg-none color-blue-main">
              Thời hạn
              <br>
              1 tháng
            </p>


            <hr class="d-none d-md-block" />
            <div class="d-none d-lg-block">
              <p class="pb-1 media-text-right color-blue-main">
                05
                <span class="absolute-text-right text-dark">
                  Truy cập kho DIY
                </span>
              </p>
              <p class="circle media-text-right background-green circle-margin-custom">
                <span class="absolute-text-right">
                  Hội viên Wowpik
                </span>
              </p>
              <p class="circle media-text-right background-green circle-margin-custom">
                <span class="absolute-text-right">
                  Dồ chơi chính hãng
                </span>
              </p>
              <p class="circle media-text-right background-green circle-margin-custom">
                <span class="absolute-text-right">
                  Tham gia sự kiện
                </span>
              </p>
              <p class="circle media-text-right background-green-opacity circle-margin-custom">
                <span class="absolute-text-right">
                  Gặp gỡ chuyên gia
                </span>
              </p>
              <p class="circle media-text-right background-green-opacity circle-margin-custom">
                <span class="absolute-text-right">
                  Quà tặng sinh nhật
                </span>
              </p>
              <p class="circle media-text-right background-green-opacity circle-margin-custom">
                <span class="absolute-text-right">
                  Tư vẫn miễn phí
                </span>
              </p>
              <p class="media-text-right">
                8.000.000đ ++
                <span class="absolute-text-right">
                  Giá trị nhận được mỗi tháng
                </span>
              </p>
            </div>
            <p data-name="YAY!" data-value="yay" data-price="1.200.000đ / tháng" data-period="Thời hạn: 1 tháng"
              data-image="./assets/images/KhamPha/yay.jpg" class="pricing-option-btn yay btn background-green mt-4"
              style="color: white;">
              Chọn</p>
          </div>
        </div>

      </div>
      <div class="col-4 col-lg-3 goiDichVu-style">
        <div class="card discovery-surprise">
          <img class="card-img-top" src="./assets/images/KhamPha/yeah.jpg" alt="Card image cap" />
          <div class="card-body card-body--styling">
            <h5 class="card-title font-VLBoosterNextFYBlack color-blue-main yyw-font">
              YEAH!
            </h5>
            <p class="card-text color-blue-main pricing font-weight-bold mb-0 d-none d-lg-block">
              990.000đ
              <span class="absolute-text-right d-lg-none text-dark font-weight-normal">
                Chi phí
              </span>
            </p>
            <p class="d-none d-lg-block  card-text color-blue-main pricing text-right text-md-center">
              06 tháng
              <span class="absolute-text-right d-lg-none text-dark">
                Thời hạn
              </span>
            </p>

            <!-- Hiển thị ở mobile -->
            <p class="d-block d-lg-none color-blue-main">
              Chi phí
              <br>
              990.000đ
            </p>
            <p class="d-block d-lg-none color-blue-main">
              Thời hạn
              <br>
              06 tháng
            </p>
            <hr class="d-none d-md-block" />
            <div class="d-none d-lg-block">
              <p class="pb-1 media-text-right color-blue-main">
                20<span class="absolute-text-right d-lg-none">
                  Truy cập kho DIY
                </span>
              </p>
              <p class="circle media-text-right background-blue circle-margin-custom">
                <span class="absolute-text-right d-lg-none">
                  Hội viên Wowpik
                </span>
              </p>
              <p class="circle media-text-right background-blue circle-margin-custom">
                <span class="absolute-text-right d-lg-none">
                  Dồ chơi chính hãng
                </span>
              </p>
              <p class="circle media-text-right background-blue circle-margin-custom">
                <span class="absolute-text-right d-lg-none">
                  Tham gia sự kiện
                </span>
              </p>
              <p class="circle media-text-right background-blue circle-margin-custom">
                <span class="absolute-text-right d-lg-none">
                  Gặp gỡ chuyên gia
                </span>
              </p>
              <p class="circle media-text-right background-blue-opacity circle-margin-custom">
                <span class="absolute-text-right d-lg-none">
                  Quà tặng sinh nhật
                </span>
              </p>
              <p class="circle media-text-right background-blue-opacity circle-margin-custom">
                <span class="absolute-text-right d-lg-none">
                  Tư vẫn miễn phí
                </span>
              </p>
              <p class="media-text-right">
                30.000.000đ ++
                <span class="absolute-text-right d-lg-none">
                  Giá trị nhận được mỗi tháng
                </span>
              </p>
            </div>

            <p data-name="YEAH!" data-value="yeah" data-price="990.000đ / tháng" data-period="Thời hạn: 6 tháng"
              data-image="./assets/images/KhamPha/yeah.jpg" class="pricing-option-btn yeah btn background-blue mt-4"
              style="color: white;">
              Chọn</p>

          </div>
        </div>
      </div>
      <div class="col-4 col-lg-3 goiDichVu-style">
        <div>
          <div class="background-blue p-2 rounded-circle best-solution" style="width:4rem; height:4rem; right:-1rem;">
            <div class="border w-100 h-100 d-flex justify-content-center align-items-center rounded-circle">
              <p class="text-white m-0" style="font-size: 0.5rem">Lựa chọn <br> tốt nhất</p>
            </div>
          </div>
        </div>
        <div class="card discovery-surprise">
          <img class="card-img-top" src="./assets/images/KhamPha/wow.jpg" alt="Card image cap" />
          <div class="card-body card-body--styling">
            <h5 class="card-title font-VLBoosterNextFYBlack color-red-main yyw-font">
              WOW!
            </h5>
            <p class="card-text color-blue-main pricing font-weight-bold mb-0 d-none d-lg-block">
              790.000đ
              <span class="absolute-text-right d-lg-none text-dark font-weight-normal ">
                Chi phí
              </span>
            </p>
            <p class="d-none d-lg-block card-text color-blue-main pricing text-right text-md-center">
              12 tháng
              <span class="absolute-text-right d-lg-none text-dark">
                Thời hạn
              </span>
            </p>
            <!-- Hiển thị ở mobile -->
            <p class="d-block d-lg-none color-blue-main">
              Chi phí
              <br>
              790.000đ
            </p>
            <p class="d-block d-lg-none color-blue-main">
              Thời hạn
              <br>
              12 tháng
            </p>
            <hr class="d-none d-md-block" />
            <div class="d-none d-lg-block">
              <p class="pb-1 media-text-right index-pricing-wow color-blue-main">
                Không giới hạn
                <span class="absolute-text-right d-lg-none text-dark">
                  Truy cập kho DIY
                </span>
              </p>
              <p class="circle media-text-right background-red circle-margin-custom">
                <span class="absolute-text-right d-lg-none">
                  Hội viên Wowpik
                </span>
              </p>
              <p class="circle media-text-right background-red circle-margin-custom">
                <span class="absolute-text-right d-lg-none">
                  Dồ chơi chính hãng
                </span>
              </p>
              <p class="circle media-text-right background-red circle-margin-custom">
                <span class="absolute-text-right d-lg-none">
                  Tham gia sự kiện
                </span>
              </p>
              <p class="circle media-text-right background-red circle-margin-custom">
                <span class="absolute-text-right d-lg-none">
                  Gặp gỡ chuyên gia
                </span>
              </p>
              <p class="circle media-text-right background-red circle-margin-custom">
                <span class="absolute-text-right d-lg-none">
                  Quà tặng sinh nhật
                </span>
              </p>
              <p class="circle media-text-right background-red circle-margin-custom">
                <span class="absolute-text-right d-lg-none">
                  Tư vẫn miễn phí
                </span>
              </p>
              <p class="media-text-right">
                60.000.000đ ++
                <span class="absolute-text-right d-lg-none">
                  Giá trị nhận được tương ứng
                </span>
              </p>
            </div>
            <p data-name="WOW!" data-value="wow" data-price="790.000đ / tháng" data-period="Thời hạn: 12 tháng"
              data-image="./assets/images/KhamPha/wow.jpg" class="pricing-option-btn wow btn background-red mt-4"
              style="color: white;">
              Chọn
            </p>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="pt-4 text-left blockPricingSelected" style="display:none">
  <p class="text-uppercase text-left text-black-50 " style="width: 90%;">
    Các gói dịch vụ đã chọn <a href="#" class="reset-pricing-option"> Chọn lại </a>
  </p>
  <div class="card mb-3" style="max-width: 540px; border: none;">
    <div class="row no-gutters">
      <div class="col-md-4">
        <img src="/assets/images/KhamPha/wow.jpg" class="card-img" alt="...">
      </div>
      <div class="col-md-8">
        <div class="card-body">
          <h4 class="card-title color-blue-main font-weight-bold">WOW!</h4>
          <p class="card-text card-price color-blue-main  ">790.000đ/thán</p>
          <p class="card-text card-period color-blue-main ">Thời hạn: 12 tháng</p>
        </div>
      </div>
    </div>
  </div>
</div>
