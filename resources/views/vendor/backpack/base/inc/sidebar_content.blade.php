{{--<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="nav-icon la la-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>--}}

{{--<li class="nav-title">First-Party Packages</li>--}}
{{--<li class="nav-item nav-dropdown">--}}
{{--    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-newspaper-o"></i> News</a>--}}
{{--    <ul class="nav-dropdown-items">--}}
{{--      <li class="nav-item"><a class="nav-link" href="{{ backpack_url('article') }}"><i class="nav-icon la la-newspaper-o"></i> <span>Articles</span></a></li>--}}
{{--      <li class="nav-item"><a class="nav-link" href="{{ backpack_url('category') }}"><i class="nav-icon la la-list"></i> <span>Categories</span></a></li>--}}
{{--      <li class="nav-item"><a class="nav-link" href="{{ backpack_url('tag') }}"><i class="nav-icon la la-tag"></i> <span>Tags</span></a></li>--}}
{{--    </ul>--}}
{{--</li>--}}

{{--<li class="nav-item"><a class="nav-link" href="{{ backpack_url('page') }}"><i class="nav-icon la la-file-o"></i> <span>Pages</span></a></li>--}}
{{--<li class="nav-item"><a class="nav-link" href="{{ backpack_url('menu-item') }}"><i class="nav-icon la la-list"></i> <span>Menu</span></a></li>--}}

{{--<!-- Users, Roles Permissions -->--}}
{{--<li class="nav-item nav-dropdown">--}}
{{--  <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-group"></i> Authentication</a>--}}
{{--  <ul class="nav-dropdown-items">--}}
{{--    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>--}}
{{--    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-group"></i> <span>Roles</span></a></li>--}}
{{--    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Permissions</span></a></li>--}}
{{--  </ul>--}}
{{--</li>--}}

{{--<li class="nav-item nav-dropdown">--}}
{{--    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-cogs"></i> Advanced</a>--}}
{{--    <ul class="nav-dropdown-items">--}}
{{--      <li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}"><i class="nav-icon la la-files-o"></i> <span>File manager</span></a></li>--}}
{{--      <li class="nav-item"><a class="nav-link" href="{{ backpack_url('backup') }}"><i class="nav-icon la la-hdd-o"></i> <span>Backups</span></a></li>--}}
{{--      <li class="nav-item"><a class="nav-link" href="{{ backpack_url('log') }}"><i class="nav-icon la la-terminal"></i> <span>Logs</span></a></li>--}}
{{--      <li class="nav-item"><a class="nav-link" href="{{ backpack_url('setting') }}"><i class="nav-icon la la-cog"></i> <span>Settings</span></a></li>--}}
{{--    </ul>--}}
{{--</li>--}}

{{--<li class="nav-title">Demo Entities</li>--}}
{{--<li class="nav-item"><a class="nav-link" href="{{ backpack_url('monster') }}"><i class="nav-icon la la-optin-monster"></i> <span>Monsters</span></a></li>--}}
{{--<li class="nav-item"><a class="nav-link" href="{{ backpack_url('icon') }}"><i class="nav-icon la la-info-circle"></i> <span>Icons</span></a></li>--}}
{{--<li class="nav-item"><a class="nav-link" href="{{ backpack_url('product') }}"><i class="nav-icon la la-shopping-cart"></i> <span>Products</span></a></li>--}}
{{--<li class="nav-item"><a class="nav-link" href="{{ backpack_url('fluent-monster') }}"><i class="nav-icon la la-pastafarianism"></i> <span>Fluent Monsters</span></a></li>--}}
{{--<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dummy') }}"><i class="nav-icon la la-poo"></i> <span>Dummies</span></a></li>--}}

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-cogs"></i>Sliders</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('slider') }}'><i class='nav-icon la la-newspaper-o'></i> Sliders</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('slidergroup') }}'><i class='nav-icon la la-newspaper-o'></i> Slidergroups</a></li>
    </ul>
</li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-cogs"></i>Home Settings</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ url('admin/setting/12/edit') }}'><i class='nav-icon la la-question'></i>Introduce Header</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ url('admin/setting/1/edit') }}'><i class='nav-icon la la-question'></i>Toys Block Header</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ url('admin/setting/2/edit') }}'><i class='nav-icon la la-question'></i>Brands Block Header</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ url('admin/setting/3/edit') }}'><i class='nav-icon la la-question'></i>Toys Countries Header</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ url('admin/setting/4/edit') }}'><i class='nav-icon la la-question'></i>Pricing Block Header</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ url('admin/setting/5/edit') }}'><i class='nav-icon la la-question'></i>Testimonial Block Header</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ url('admin/setting/6/edit') }}'><i class='nav-icon la la-newspaper-o'></i>Activities Block Header</a></li>
    </ul>
</li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-cogs"></i>Home Blocks</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('toys') }}'><i class='nav-icon la la-newspaper-o'></i> Toys</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('brands') }}'><i class='nav-icon la la-newspaper-o'></i> Brands</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('countries') }}'><i class='nav-icon la la-newspaper-o'></i> Countries</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('activities') }}'><i class='nav-icon la la-newspaper-o'></i> Activities</a></li>
    </ul>
</li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-cogs"></i>Service Settings</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ url('admin/setting/7/edit') }}'><i class='nav-icon la la-question'></i>Service 3 Steps Block Title</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ url('admin/setting/8/edit') }}'><i class='nav-icon la la-question'></i>Service Rewards Block Title</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ url('admin/setting/9/edit') }}'><i class='nav-icon la la-question'></i>Service Rewards Block Short Desc</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ url('admin/setting/10/edit') }}'><i class='nav-icon la la-question'></i>Service Preservation Block Title</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ url('admin/setting/11/edit') }}'><i class='nav-icon la la-question'></i>Service Exports Block Title</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ url('admin/setting/17/edit') }}'><i class='nav-icon la la-question'></i>Service Expert Background</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('serviceitem') }}'><i class='nav-icon la la-newspaper-o'></i>Service Items</a></li>
    </ul>
</li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-cogs"></i>Products</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('tproduct') }}'><i class='nav-icon la la-question'></i> TProducts</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('tproductbrand') }}'><i class='nav-icon la la-question'></i> TProduct Brands</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('tproductage') }}'><i class='nav-icon la la-question'></i> TProduct Ages</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('tproductmaterial') }}'><i class='nav-icon la la-question'></i> TProduct Materials</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('tproducttype') }}'><i class='nav-icon la la-question'></i> TProduct Types</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('review') }}'><i class='nav-icon la la-question'></i> Reviews</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-cogs"></i>Blogs</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('blog') }}'><i class='nav-icon la la-question'></i> Blogs</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('bcategory') }}'><i class='nav-icon la la-question'></i> BCategories</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('bperiod') }}'><i class='nav-icon la la-question'></i> BPeriods</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-cogs"></i>Diy</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('diypost') }}'><i class='nav-icon la la-question'></i> Diy Posts</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('diycategory') }}'><i class='nav-icon la la-question'></i> Diy Categories</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('diytheme') }}'><i class='nav-icon la la-question'></i> Diy Themes</a></li>
    </ul>
</li>


<li class='nav-item'><a class='nav-link' href='{{ backpack_url('registrations') }}'><i class='nav-icon la la-question'></i> Registrations</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('registrations_gift') }}'><i class='nav-icon la la-question'></i> Registration Gifts</a></li>